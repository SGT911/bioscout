import toml
from os import environ as env, getcwd
from os.path import join as path_join

FILE = env.get("CONFIG_FILE", path_join(getcwd(), "config.toml"))

def load_from_env(data: dict) -> dict:
	n_data = dict()
	for k, v in data.items():
		if isinstance(v, dict):
			n_data[k] = load_from_env(v)
		elif isinstance(v, str) and v.startswith('$'):
			val = env.get(v[1:], None)
			if isinstance(val, str) and val.isnumeric():
				val = int(val)
			n_data[k] = val
		else:
			n_data[k] = v

	return n_data

DATA = None
with open(FILE, 'r') as f:
	DATA = load_from_env(toml.loads(f.read()))