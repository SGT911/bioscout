class InternalServerError(Exception):
	pass


class NotFound(Exception):
	pass


class MetaError(Exception):
	pass