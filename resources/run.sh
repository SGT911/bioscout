#!/bin/bash

APP_DIR=$(dirname $0)

cd $APP_DIR
if [ -d $APP_DIR/.venv ]; then
	source $APP_DIR/.venv/bin/activate
	CONFIG_FILE=$1 python -m server
	deactivate
else
	CONFIG_FILE=$1 python -m server
fi
