import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import Vuex from '/static/cdn/vuex/vuex.esm.browser.min.js'

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		notifications: new Array()
	},
	mutations: {
		add(state, {
			title,
			type,
			description
		}) {
			state.notifications.push({
				id: state.notifications.length,
				title,
				type,
				description: description || null
			})
		},
		destroy(state, idx) {
			state.notifications = state.notifications.filter((el, i) => i != idx).map((el, i) => {
				return {
					...el,
					id: i
				}
			})
		},
		clear(state) {
			state.notifications = []
		}
	}
})

export default new Vue({
	name: 'Notify',
	store: store,
	computed: {
		...Vuex.mapState(['notifications'])
	},
	methods: {
		...Vuex.mapMutations(['destroy'])
	},
	template: /*html*/
	`
		<div class="notifications-stack">
			<div
				v-for="noti in notifications"
				v-bind:key="noti.id"
				class="message"
				v-bind:class="{
					[\`is-\${noti.type}\`]: noti.type !== null
				}"
			>
				<div class="message-header">
					<p>{{ noti.title }}</p>
					<button
						class="delete"
						aria-label="delete"
						v-on:click="destroy(noti.id)"
					></button>
				</div>
				<div
					class="message-body"
					v-if="noti.description !== null"
				>
					{{ noti.description }}
				</div>
			</div>
		</div>
	`
})