window.addEventListener('load', () => {
	const els = document.getElementsByClassName('navbar-burger')

	for (var i = 0; i < els.length; i++) {
		const el = els[i]
		const target = document.getElementById(el.dataset.target)

		window.addEventListener('resize', () => {
			if (1024 > window.innerWidth) {
				target.style.display = 'none'
			} else {
				target.style.display = 'flex'
			}
		})

		el.onclick = () => {
			if (target.style.display == '' || target.style.display == 'none') {
				target.style.display = 'block'
			} else {
				target.style.display = 'none'
			}
		}
	}
})