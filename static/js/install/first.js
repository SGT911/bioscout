'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import {
	nullDirective,
	numericDirective,
	capitalizeDirective
} from '/static/js/directives.js'

import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'

class Fork {
	constructor({ type, name }) {
		this.type = type
		this.name = name
	}

	static default() {
		return new Fork({
			type: null,
			name: null
		})
	}
}

Vue.directive(nullDirective.name, nullDirective.directive)
Vue.directive(numericDirective.name, numericDirective.directive)
Vue.directive(capitalizeDirective.name, capitalizeDirective.directive)

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	data: {
		groupNumber: null,
		groupName: null,
		forks: []
	},
	methods: {
		async addFork(forkField, value) {
			this.$data.forks.push(new Fork({
				...Fork.default(),
				[forkField]: value
			}))

			await this.$forceUpdate()

			let id = this.$data.forks.length - 1

			this.$refs[`pre-${forkField}`].value = null
			this.$refs[`${forkField}-${id}`][0].focus()
		},
		checkFull(idx) {
			if (
				(this.$data.forks[idx].type == null || this.$data.forks[idx].type == '')
				&&
				(this.$data.forks[idx].name == null || this.$data.forks[idx].name == '')
			) {
				this.$data.forks = this.$data.forks.filter((el, i) => i != idx)
				this.$refs[`pre-type`].focus()
			}
		},
		clear() {
			this.$data.groupNumber = null
			this.$data.groupName = null
			this.$data.forks = []
		},
		check() {
			if (this.$data.groupNumber == null || this.$data.groupName == null) return false

			const checkFork = el => el.type == null || el.name == null || el.type == '' || el.name == ''
			if (this.$data.forks.length == 0 || this.$data.forks.find(checkFork) != undefined) return false

			return true
		},
		async save() {
			if (!this.check()) {
				return notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Todos los campos son requeridos.'
				})
			}

			let data = {
				group_number: this.$data.groupNumber,
				group_name: this.$data.groupName.toLowerCase(),
				forks: this.$data.forks.map(el => {
					return {
						type: el.type.toLowerCase(),
						name: el.name.toLowerCase()
					}
				})
			}

			let req = await fetch('/api/install/first', {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify(data)
			}).then(out => out.json())

			if (req.errors.length > 0) {
				console.error(req.errors)
				return notifyStore.commit('add', {
					type: 'danger',
					title: 'Error en el Servidor',
					description: 'Error de conectividad o tratamiento de datos'
				})
			}

			notifyStore.commit('add', {
				type: 'success',
				title: 'Datos guardados'
			})

			return window.location.replace('/install/step/second')
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}