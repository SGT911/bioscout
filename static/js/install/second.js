'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import {
	nullDirective,
	numericDirective,
	capitalizeDirective,
	maxLengthDirective
} from '/static/js/directives.js'
import { emailRegex } from '/static/js/lib/regex.js'

import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'
import datePicker, { parseDate } from '/static/js/components/datePicker.js'

Vue.directive(nullDirective.name, nullDirective.directive)
Vue.directive(numericDirective.name, numericDirective.directive)
Vue.directive(capitalizeDirective.name, capitalizeDirective.directive)
Vue.directive(maxLengthDirective.name, maxLengthDirective.directive)

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	data: {
		showPassword: false,

		niType: null,
		ni: null,
		fullName: null,
		birthday: new Date(),
		email: null,
		phone: null,
		fork: null,

		password: null,
		passwordConfirm: null
	},
	components: {
		datePicker
	},
	methods: {
		check() {
			if (
				this.$data.niType         == null ||
				this.$data.ni             == null ||
				this.$data.fullName        == null ||
				this.$data.email           == null ||
				this.$data.phone           == null ||
				this.$data.fork            == null ||
				this.$data.password        == null ||
				this.$data.passwordConfirm == null
			) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Todos los campos son requeridos',
				})
				return false
			}

			if (this.$data.password != this.$data.passwordConfirm) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Las contraseñas no coinciden',
				})
				return false
			}

			let diffDate = (new Date() - this.$data.birthday) / (1000 * 60 * 60 * 24 * 365)
			if (diffDate < 18) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'La fecha no es valida',
				})
				return false
			}

			if (!emailRegex.test(this.$data.email)) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'El correo no es valido',
				})
				return false
			}

			return true
		},
		async send() {
			if (!this.check()) return null

			const data = {
				ni_type: this.$data.niType,
				ni: this.$data.ni,
				full_name: this.$data.fullName.toLowerCase(),
				birthday: parseDate(this.$data.birthday),
				email: this.$data.email,
				phone: this.$data.phone,
				fork: this.$data.fork,
				password: this.$data.password
			}

			let req = await fetch('/api/install/second', {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify(data)
			}).then(out => out.json())

			if (req.errors.length > 0) {
				console.error(req.errors)
				return notifyStore.commit('add', {
					type: 'danger',
					title: 'Error en el Servidor',
					description: 'Error de conectividad o tratamiento de datos'
				})
			}

			notifyStore.commit('add', {
				type: 'success',
				title: 'Datos guardados, ahor se necesita inicio de sesion'
			})

			return window.location.replace('/')
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}