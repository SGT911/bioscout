import { capitalize, upper, reduceInt } from '/static/js/lib/parsing.js'
import {
	vueFindDirective as findDirective,
	vueModifyData as modifyData,
	vueParseString as parseString
} from '/static/js/lib/vue.js'

export const nullDirective = {
	name: 'null',
	directive: {
		componentUpdated(el, bind, vnode) {
			let VModel = findDirective(vnode.data.directives || [], 'model')

			if (VModel == undefined) return
			if (VModel.value == '') {
				el.value = ''
				modifyData(vnode.context.$data, parseString(VModel.expression, bind.value), null)
			}
		}
	}
}

export const numericDirective = {
	name: 'numeric',
	directive: {
		componentUpdated(el, bind, vnode) {
			let VModel = findDirective(vnode.data.directives || [], 'model')
			let val = null

			if (VModel == undefined) return
			if (VModel.value != '') {
				val = reduceInt(VModel.value)
				el.value = val
			} else {
				el.value = ''
				val = null
			}

			if (VModel.value !== val) {
				modifyData(vnode.context.$data, parseString(VModel.expression, bind.value), val)
			}
		}
	}
}

export const capitalizeDirective = {
	name: 'capital',
	directive: {
		componentUpdated(el, bind, vnode) {
			let VModel = findDirective(vnode.data.directives || [], 'model')

			if (VModel == undefined) return
			if (VModel.value != '') {
				let val = capitalize(VModel.value)

				if (VModel.value != val) {
					el.value = val
					modifyData(vnode.context.$data, parseString(VModel.expression, bind.value), val)
				}
			}
		}
	}
}

export const upperDirective = {
	name: 'upper',
	directive: {
		componentUpdated(el, bind, vnode) {
			let VModel = findDirective(vnode.data.directives || [], 'model')

			if (VModel == undefined) return
			if (VModel.value != '') {
				let val = upper(VModel.value)

				if (VModel.value != val) {
					el.value = val
					modifyData(vnode.context.$data, parseString(VModel.expression, bind.value), val)
				}
			}
		}
	}
}

export const maxLengthDirective = {
	name: 'max-length',
	directive: {
		componentUpdated(el, bind, vnode) {
			let VModel = findDirective(vnode.data.directives || [], 'model')

			if (VModel == undefined) return
			if (VModel.value != '' && VModel.value !== null) {
				let val = VModel.value.toString().substr(0, bind.value)

				if (VModel.value instanceof Number) {
					val = parseInt(val)
				}

				if (VModel.value != val) {
					el.value = val
					modifyData(vnode.context.$data, parseString(VModel.expression, bind.value), val)
				}
			}
		}
	}
}