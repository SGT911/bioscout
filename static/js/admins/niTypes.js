'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import { capitalizeDirective, upperDirective } from '/static/js/directives.js'
import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'

Vue.directive(capitalizeDirective.name, capitalizeDirective.directive)
Vue.directive(upperDirective.name, upperDirective.directive)

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	data: {
		showSummary: false,

		abbreviation: null,
		name: null
	},
	watch: {
		abbreviation() {
			if (this.$data.abbreviation != null) {
				this.$data.abbreviation = this.$data.abbreviation
					.replace(/\ /g, '')
					.substr(0, 6)
			}
		}
	},
	methods: {
		check() {
			if (
				this.$data.abbreviation == null ||
				this.$data.name == null
			) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Todos los campos son olbigatorios.'
				})
				return false
			}

			return true
		},
		send(event) {
			if (!this.check()) return event.preventDefault()
		}
	}
})

window.addEventListener('load', start)
function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}