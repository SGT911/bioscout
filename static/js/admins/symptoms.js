'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import { capitalizeDirective } from '/static/js/directives.js'
import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'

Vue.directive(capitalizeDirective.name, capitalizeDirective.directive)

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	data: {
		showSummary: false,

		description: null
	},
	methods: {
		check() {
			if (this.$data.description == null) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'El campo es olbigatorio.'
				})
				return false
			}

			return true
		},
		send(event) {
			if (!this.check()) return event.preventDefault()
		}
	}
})

window.addEventListener('load', start)
function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}