'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import datePicker, { parseDate } from '/static/js/components/datePicker.js'

const appEl = document.getElementById('filter')
const app = new Vue({
	components: {
		datePicker
	},
	data: {
		show: false,

		fork: null,
	},
	methods: {
		apply() {
			if (this.$data.fork != null) {
				let { origin, pathname } = window.location

				window.location.replace(`${origin}${pathname}?fork=${this.$data.fork}`)
			}
		},
		reset() {
			let { origin, pathname } = window.location

			window.location.replace(`${origin}${pathname}`)	
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
}