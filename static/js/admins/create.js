'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	data: {
		filter: null,

		admin: null
	},
	methods: {
		check() {
			if (this.$data.admin == null) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'El campo es olbigatorio.'
				})
				return false
			}

			return true
		},
		send(event) {
			if (!this.check()) return event.preventDefault()
		},
		runFilter() {
			window.location.replace(`/admin/create?fork=${ this.$data.filter }`)
		}
	}
})

window.addEventListener('load', start)
function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}