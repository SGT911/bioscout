'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'

function createVueComponent() {
	return new Vue({
		data: {
			show: false
		}
	})
}

window.addEventListener('load', start)

function start() {
	let els = document.getElementsByClassName("desplegable")
	for (var i = 0; i < els.length; i++) {
		const el = els[i]
		const component = createVueComponent()
		component.$mount(el)
	}

	els = document.getElementsByClassName("copyable")
	for (var i = 0; i < els.length; i++) {
		const el = els[i]
		
		el.onclick = () => {
			try {
				navigator.clipboard.writeText(el.innerText)
			} catch(e) {
				let input = document.createElement('input')
				input.value = el.innerText

				document.body.appendChild(input)
				input.select(0, el.innerText.length)

				document.execCommand('copy')
				input.remove()

			}

			alert('Copiado al porta papeles')
		}
	}
}