'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import datePicker, { parseDate } from '/static/js/components/datePicker.js'

const appEl = document.getElementById('filter')
const app = new Vue({
	components: {
		datePicker
	},
	data: {
		show: false,

		fork: null,
		from: new Date(),
		applyFrom: false,
		to: new Date(),
		applyTo: false,
	},
	watch: {
		from() {
			this.$data.applyFrom = parseDate(this.$data.from) != parseDate(new Date())
		},
		to() {
			this.$data.applyTo = parseDate(this.$data.to) != parseDate(new Date())
		}
	}, 
	methods: {
		apply() {
			let data = {}
			if (this.$data.fork != null) {
				data['fork'] = this.$data.fork.toString()
			}

			if (this.$data.applyFrom) {
				data['date_from'] = parseDate(this.$data.from)
			}

			if (this.$data.applyTo) {
				data['date_to'] = parseDate(this.$data.to)
			}

			let query = []
			for (const key in data) {
				query.push(`${key}=${data[key]}`)
			}

			query = '?' + query.join('&')
			query = (query == '?')? '' : query

			let { origin, pathname } = window.location

			window.location.replace(`${origin}${pathname}${query}`)
		},
		reset() {
			let { origin, pathname } = window.location

			window.location.replace(`${origin}${pathname}`)	
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
}