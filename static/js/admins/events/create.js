'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import {
	numericDirective,
	nullDirective,
	capitalizeDirective
} from '/static/js/directives.js'
import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'
import datePicker, { parseDate } from '/static/js/components/datePicker.js'

Vue.directive(numericDirective.name, numericDirective.directive)
Vue.directive(nullDirective.name, nullDirective.directive)
Vue.directive(capitalizeDirective.name, capitalizeDirective.directive)

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	components: {
		datePicker
	},
	mounted() {
		this.$data.ni = parseInt(this.$refs.ni.dataset.value)
	},
	data: {
		ni: null,
		name: null,
		date: new Date(),
		forks: []
	},
	methods: {
		altern(id, add) {
			if (add) {
				this.$data.forks.push(id)
			} else {
				this.$data.forks = this.$data.forks.filter(v => v != id)
			}
		},
		check() {
			if (this.$data.ni == null || this.$data.name == null || this.$data.forks.length == 0) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Todos los campos son obligatorios'
				})

				return false
			}

			if (this.$data.date <= new Date()) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'La fecha no es valida'
				})

				return false
			}

			return true
		},
		async send() {
			if (!this.check()) return null
			let data = {
				...this.$data,
				date: parseDate(this.$data.date),
				name: this.$data.name.toLowerCase()
			}
			
			let req = await fetch('/api/events', {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify(data)
			}).then(out => out.json())

			if (req.errors.length > 0) {
				console.error(req.errors)
				return notifyStore.commit('add', {
					type: 'danger',
					title: 'Error en el Servidor',
					description: 'Error de conectividad o tratamiento de datos'
				})
			}

			notifyStore.commit('add', {
				type: 'success',
				title: 'Actividad Creada'
			})

			return window.location.replace(`/events/${req.payload}`)
		},
		reset() {
			this.$data.name = null
			this.$data.date = new Date()
			this.$data.forks = []
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}