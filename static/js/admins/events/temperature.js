'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import { reduceFloat, reduceInt } from '/static/js/lib/parsing.js'

const appEl = document.getElementById('app')

const app = new Vue({
	mounted() {
		this.$data.event = parseInt(this.$refs.event.dataset.value)
		this.$data.filter = reduceInt(this.$refs.filter.dataset.value)

		const regex = /^input-(\w+)-(\d+)$/
		for (const key in this.$refs) {
			if (this.$refs.hasOwnProperty(key) && regex.test(key)) {
				const value = this.$refs[key]
				let [inputKey, ni] = regex.exec(key).slice(1)
				ni = parseInt(ni)

				if (!this.$data.subjects.hasOwnProperty(ni)) {
					this.$data.subjects[ni] = {}
				}

				if (value.dataset.value != 0) {
					value.value = parseFloat(value.dataset.value)
					this.$data.subjects[ni][inputKey] = parseFloat(value.dataset.value)
				} else {
					value.value = ''
					this.$data.subjects[ni][inputKey] = null
				}
			}
		}
	},
	data: {
		event: null,
		filter: null,
		subjects: {}
	},
	methods: {
		async send() {
			const data = {
				event: this.$data.event,
				subjects: {}
			}

			for (const key in this.$data.subjects) {
				data.subjects[key.toString()] = this.$data.subjects[key]
			}

			let req = await fetch('/api/signup/temperatures', {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify(data)
			}).then(out => out.json())

			if (req.errors.length > 0) {
				console.error(req.errors)
				return alert('Error conectando con la base de datos')
			}

			alert('Datos guardados exitosamente')
		},
		runFilter() {
			window.location.replace(`/events/${ this.$data.event }/temperature?fork=${ this.$data.filter }`)
		},
		inputSubject(ni, key, element) {
			let value = reduceFloat(element.value)

			this.$data.subjects[ni][key] = value
			if (element.value.endsWith('.')) {
				element.value = `${value}.`
			} else {
				element.value = value
			}
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
}