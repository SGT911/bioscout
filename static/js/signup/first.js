'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import {
	nullDirective,
	numericDirective,
	capitalizeDirective,
	maxLengthDirective
} from '/static/js/directives.js'
import { emailRegex } from '/static/js/lib/regex.js'

import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'
import datePicker, { parseDate } from '/static/js/components/datePicker.js'

Vue.directive(nullDirective.name, nullDirective.directive)
Vue.directive(numericDirective.name, numericDirective.directive)
Vue.directive(capitalizeDirective.name, capitalizeDirective.directive)
Vue.directive(maxLengthDirective.name, maxLengthDirective.directive)

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	mounted() {
		this.$data.fork = parseInt(this.$refs['fork'].dataset.value)
		this.$data.eventId = parseInt(this.$refs['event'].value)

		if (this.$refs['ni'].dataset.value != 'null') {
			this.$data.ni = parseInt(this.$refs['ni'].dataset.value)
		}
	},
	data: {
		created: false,
		eventId: null,

		niType: null,
		ni: null,
		fullName: null,
		birthday: new Date(),
		email: null,
		phone: null,
		fork: null,

		parentNiType: null,
		parentNi: null,
		parentFullName: null,
		parentEmail: null,
		parentPhone: null
	},
	watch: {
		async parentNi() {
			if (
				this.$data.parentNi !== null &&
				typeof this.$data.parentNi != 'string' &&
				this.$data.parentNi.toString().length >= 6
			) {
				let data = await fetch(`/api/subjects/parent/${this.$data.parentNi}`).then(out => out.json())
				if (data.status == 'ok') {
					let { payload } = data

					this.$data.parentNiType = payload.ni_type.id
					this.$data.parentNi = payload.ni
					this.$data.parentFullName = payload.full_name
					this.$data.parentEmail = payload.email
					this.$data.parentPhone = payload.phone
				}
			}
		},
		async ni() {
			if (
				this.$data.ni !== null &&
				typeof this.$data.ni != 'string' &&
				this.$data.ni.toString().length >= 6
			) {
				let data = await fetch(`/api/subjects/${this.$data.ni}`).then(out => out.json())
				if (data.status == 'ok') {
					let { payload } = data
					this.$data.created = payload.is_subject

					if (payload.is_subject && payload.fork.id != this.$data.fork) {
						return window.location.replace(`/signup/${this.$data.eventId}-${payload.fork.id}?ni=${payload.ni}`)
					}

					this.$data.niType = payload.ni_type.id
					this.$data.fullName = payload.full_name
					this.$data.email = payload.email
					this.$data.phone = payload.phone

					let [year, month, day] = payload.birthday.split('-')
					this.$data.birthday = new Date(year, month - 1, day)

					if (payload.parent != null) {
						let { parent } = payload
						this.$data.parentNiType = parent.ni_type.id
						this.$data.parentNi = parent.ni
						this.$data.parentFullName = parent.full_name
						this.$data.parentEmail = parent.email
						this.$data.parentPhone = parent.phone
					} else {
						this.$data.parentNiType = null
						this.$data.parentNi = null
						this.$data.parentFullName = null
						this.$data.parentEmail = null
						this.$data.parentPhone = null
					}

					this.$forceUpdate()
				}

				return null
			}

			this.$data.created = false

			this.$data.niType = null
			this.$data.fullName = null
			this.$data.birthday = new Date()
			this.$data.email = null
			this.$data.phone = null
			this.$data.parentNiType = null
			this.$data.parentNi = null
			this.$data.parentFullName = null
			this.$data.parentEmail = null
			this.$data.parentPhone = null
		}
	},
	components: {
		datePicker
	},
	computed: {
		needParent() {
			let diffDays = (new Date() - this.$data.birthday) / (1000 * 60 * 60 * 24)
			if (0 <= diffDays && diffDays < 1) return false

			let diffYears = diffDays / 365
			if (diffYears >= 18) return false

			return true
		}
	},
	methods: {
		check() {
			if (
				this.$data.niType  == null ||
				this.$data.ni      == null ||
				this.$data.fullName == null ||
				this.$data.email    == null ||
				this.$data.phone    == null
			) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Todos los campos son requeridos',
				})
				return false
			}

			if (this.$data.password != this.$data.passwordConfirm) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Las contraseñas no coinciden',
				})
				return false
			}

			let diffDate = (new Date() - this.$data.birthday) / (1000 * 60 * 60 * 24 * 365)
			if (diffDate < 5) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'La edad no es valida',
				})
				return false
			}

			if (this.needParent && (
				this.$data.parentNiType  == null ||
				this.$data.parentNi      == null ||
				this.$data.parentFullName == null ||
				this.$data.parentEmail    == null ||
				this.$data.parentPhone    == null
			)) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Todos los datos del acudiente son obligatorios',
				})
				return false
			}

			if (!emailRegex.test(this.$data.email)) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'El correo no es valido',
				})
				return false
			}

			return true
		},
		async send() {
			if (!this.check()) return null

			let data = {
				ni_type: this.$data.niType,
				ni: this.$data.ni,
				full_name: this.$data.fullName.toLowerCase(),
				birthday: parseDate(this.$data.birthday),
				email: this.$data.email,
				phone: this.$data.phone,
				fork: this.$data.fork
			}

			if (this.needParent) {
				data.parent = {
					ni_type: this.$data.parentNiType,
					ni: this.$data.parentNi,
					full_name: this.$data.parentFullName.toLowerCase(),
					email: this.$data.parentEmail,
					phone: this.$data.parentPhone
				}
			}

			let req = await fetch(`/api/subjects/${data.ni}`, {
				method: (this.$data.created)? 'PUT' : 'POST',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify(data)
			}).then(out => out.json())

			if (req.errors.length > 0) {
				console.error(req.errors)
				return notifyStore.commit('add', {
					type: 'danger',
					title: 'Error en el Servidor',
					description: 'Error de conectividad o tratamiento de datos'
				})
			}

			this.$data.created = true
			notifyStore.commit('add', {
				type: 'success',
				title: 'Datos guardados, ahora puede seguir con la inscripcion'
			})
		},
		next() {
			window.location.replace(`/signup/symptoms/${this.$data.eventId}-${this.$data.ni}`)
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}