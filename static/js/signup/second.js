'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import { nullDirective } from '/static/js/directives.js'
import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'

Vue.directive(nullDirective.name, nullDirective.directive)

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	mounted() {
		this.$data.eventId = parseInt(this.$refs['evt'].value)
		this.$data.ni = parseInt(this.$refs['ni'].value)
		this.$data.finished = parseInt(this.$refs['is_already'].value) == 1
	},
	data: {
		finished: false,

		ni: null,
		eventId: null,

		none: false,
		symptoms: [],
		other: null
	},
	computed: {
		noNone() {
			return this.$data.symptoms.length > 0 || this.$data.other != null
		}
	},
	methods: {
		addAfection(id, add) {
			if (add) {
				this.$data.symptoms.push(id)
			} else {
				this.$data.symptoms = this.$data.symptoms.filter(el => el != id)
			}
		},
		clear() {
			this.$data.symptoms.map(idx => {
				let el = this.$refs[`check-${idx}`]
				
				el.checked = false
			})

			this.$data.none = false
			this.$data.other = null
			this.$data.symptoms = []
		},
		check() {
			return this.$data.none || this.noNone
		},
		async send() {
			if (!this.check()) {
				return notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Todos los campos son requeridos',
				})
			}

			const data = {
				ni: this.$data.ni,
				evt: this.$data.eventId,
				other: (this.$data.other == null)? undefined : this.$data.other,
				symptoms: this.$data.symptoms
			}

			let req = await fetch('/api/signup/symptoms', {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify(data)
			}).then(out => out.json())

			if (req.errors.length > 0) {
				console.error(req.errors)
				return notifyStore.commit('add', {
					type: 'danger',
					title: 'Error en el Servidor',
					description: 'Error de conectividad o tratamiento de datos'
				})
			}

			notifyStore.commit('clear')
			this.$data.finished = true
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}