export const vueFindDirective = (directives, name) => {
  for (let i = 0; i < directives.length; i++) {
    const directive = directives[i]
    if (directive.name == name) return directive
  }

  return undefined
}

export const vueModifyData = (data, endpoint, value) => {
    const route = endpoint.split('.')
    if (route.length == 0) return

    if (data.hasOwnProperty(route[0])) {
        if (route.length == 1) {
            data[route[0]] = value
        } else {
            vueModifyData(data[route[0]], route.slice(1).join('.'), value)
        }

    }
}

export const vueParseString = (route, parseValues) => {
    let res = route

    for (const k in parseValues) {
        res = res.replace(new RegExp(`\\[${k}\\]`, 'g'), `.${parseValues[k]}`)
    }

    return res
}