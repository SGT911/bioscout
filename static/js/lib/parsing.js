export function reduceInt(str) {
    if (typeof str == 'number') return str
    if (typeof str != 'string') return null

    let conv = (str || '').replace(/\D+/g, '')
    if (conv === '') {
        return null
    }

    return parseInt(conv)
}

export function reduceFloat(str) {
    if (typeof str == 'number') return str
    if (typeof str != 'string') return null

    let conv = (str || '').split('').filter(el => /[0-9\.]/.test(el)).join('')
    if (conv === '') {
        return null
    }

    return parseFloat(conv)
}

export function capitalize(str) {
    if (typeof str != 'string') return null

    return str.split(' ').map(txt => txt.substr(0, 1).toUpperCase() + txt.substr(1).toLowerCase()).join(' ')
}

export function upper(str) {
    if (typeof str != 'string') return null

    return str.toUpperCase()
}