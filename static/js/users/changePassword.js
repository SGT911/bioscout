'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	mounted() {
		this.$data.ni = parseInt(this.$refs.ni.dataset.value)
	},
	data: {
		ni: null,
		password: null,
		passwordConfirm: null,
		showPassword: false,
	},
	methods: {
		check() {
			if (
				this.$data.password == null ||
				this.$data.passwordConfirm == null
			) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Todos los campos son obligatorios'
				})
				return false
			}

			if (this.$data.password != this.$data.passwordConfirm) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Las contraseñas no coinciden'
				})
				return false
			}

		return true
		},
		async send() {
			if (!this.check()) {
				return null
			}

			const data = {
				ni: this.$data.ni,
				password: this.$data.password
			}

			let req = await fetch('/api/user/changePassword', {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				body: JSON.stringify(data)
			}).then(out => out.json())

			if (req.errors.length > 0) {
				console.error(req.errors)
				return notifyStore.commit('add', {
					type: 'danger',
					title: 'Error en el Servidor',
					description: 'Error de conectividad o tratamiento de datos'
				})
			}

			notifyStore.commit('add', {
				type: 'success',
				title: 'Contraseña cambiada'
			})

			return window.location.replace('/')
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}