'use strict'

import Vue from '/static/cdn/vue/vue.esm.browser.min.js'
import { numericDirective, nullDirective } from '/static/js/directives.js'
import Notify, { store as notifyStore } from '/static/js/helpers/notifications.js'

Vue.directive(numericDirective.name, numericDirective.directive)
Vue.directive(nullDirective.name, nullDirective.directive)

const appEl = document.getElementById('app')
const notificationEl = document.getElementById('notifications')

const app = new Vue({
	data: {
		ni: null,
		password: null,
		showPassword: false
	},
	methods: {
		check() {
			return this.$data.ni != null && this.$data.password != null
		},
		async send(evt) {
			if (!this.check()) {
				notifyStore.commit('add', {
					type: 'danger',
					title: 'Error',
					description: 'Todos los campos son requeridos.'
				})
				return evt.preventDefault()
			}
		}
	}
})

window.addEventListener('load', start)

function start() {
	app.$mount(appEl)
	Notify.$mount(notificationEl)
}