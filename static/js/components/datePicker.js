import { reduceInt } from '/static/js/lib/parsing.js'

export function parseDate(date) {
	date = new Date(date)

	let year = date.getFullYear().toString()
	let month = parseNumber(date.getMonth() + 1)
	let day = parseNumber(date.getDate())

	return `${year}-${month}-${day}`
}

export function parseNumber(num) {
	if (num >= 10) {
		return num.toString()
	}

	return `0${num}`
}

export default {
	name: 'DatePicker',
	props: {
		value: {
			type: Date,
			default: () => new Date(),
			required: false
		}
	},
	mounted() {
		let { year, month, day } =  this
		this.emitValue(year, month, day)
	},
	computed: {
		year() {
			let date = new Date(this.$props.value)
			return date.getFullYear()
		},
		month() {
			let date = new Date(this.$props.value)
			return date.getMonth()
		},
		day() {
			let date = new Date(this.$props.value)
			return date.getDate()
		}
	},
	methods: {
		getPossibleDate() {
			if (this.month == 2) {
				let { year } =  this
				if (
					(year % 400) == 0 ||
					(
						(year % 4) == 0 &&
						(year % 100) != 0
					)
				) return 29
				return 28
			}

			const daysOf31 = [1, 3, 5, 7, 8, 10, 12]
			if (daysOf31.indexOf(this.month + 1) != -1) return 31

			return 30
		},
		checkDate() {
			if (this.getPossibleDate() < this.day) {
				this.day = 1
			}
		},
		emitValue(year, month, day) {
			if (this.getPossibleDate() < day) {
				day = 1
			}

			if (year > 1900) {
				this.$emit('input', new Date(year, month, day))
			}
		},
		createArray(to) {
			const list = new Array()
			for (var i = 1; i <= to; i++) {
				list.unshift(i)
			}

			return list
		},
		parseNumber: reduceInt,
		formatNumber: parseNumber
	},
	template: /*html*/
	`
		<div class="columns is-mobile is-gapless date-picker">
			<div class="column">
				<input
					
					type="number"
					inputmode="numeric"
					placeholder="Año"
					class="input"
					v-bind:value="year"
					v-on:input="emitValue(parseNumber($event.target.value), month, day)"
				>
			</div>
			<div class="column">
				<div class="select">
					<select
						v-bind:value="month"
						v-on:input="emitValue(year, parseNumber($event.target.value), day)"
					>
						<option v-bind:value="null" disabled="true">Mes</option>
						<option v-bind:value="i - 1" v-for="i in createArray(12)" v-bind:key="i">{{ formatNumber(i) }}</option>
					</select>
				</div>
			</div>
			<div class="column">
				<div class="select">
					<select
						v-bind:value="day"
						v-on:input="emitValue(year, month, parseNumber($event.target.value))"
					>
						<option v-bind:value="null" disabled="true">Dia</option>
						<option v-bind:value="i" v-for="i in createArray(getPossibleDate())" v-bind:key="i">{{ formatNumber(i) }}</option>
					</select>
				</div>
			</div>
		</div>
	`
}