# Configuration
The configuration use a `toml` file separated in namespace as `[server]` for get the respected configuration.

```toml
# Example of the default file
[server]
	[server.web]
	active = true # Check if binding method is active

	# host and port address for binding
	host = '0.0.0.0'
	port = 8000

	[server.unixsocket]
	active = false # Check if binding method is active

	# Path and Binding mode
	file = '/tmp/bioscout.sock'
	mode = 666

[config]
# Locale and language for application
lang = 'es'
locale = 'es_CO'

# Session Cookies
session_key = 'TornadoID'
cookie_secret = '__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__'
	
	[config.files]
	temp_folder = '/tmp'

[database]
on_error_reconnect = true # Reconnect on connection error
	[database.postgres] # Database connection
	database = 'bio_scout'

	host = 'localhost'
	port = 5432

	user = 'postgres'
	password = 'postgres admin'

	[database.redis] # Session Storage connection
	host = 'localhost'
	port = 6379

	db = 0

	password = '' # Empty field for None
```

## `[server]`
This namespace have all configurations for run the server wwith unix domains or directly with a HTTP port.

+ `[server.web]`
	- `active`: Bool field, set to expose the app directly via HTTP port, By default `true`
	- `host`: String field, set the expose ip, By default `'0.0.0.0'`
	- `port`: Integer field, set the port to expose, By default `8000`

+ `[server.unixsocket]`
	- `active`: Bool field, set to expose the app via UNIX domain sockets, By default `false`
	- `file`: String field, set the file to expose, By default `'/tmp/bioscout.sock'`
	- `mode`: Integer file, set the permissions of the socket file, By default `666`

## `[config]`
This namespace set all internal app configuration as locale, cookies and others.

+ `lang`: String field, set the page lang, *NOTE:* This field only accept `ISO 3166-1` format strings.
+ `locale`: String field, set the locale files in the app, *NOTE:* This field only accept the created locales created in the repository for more details refer to [README](https://gitlab.com/SGT911/bioscout#locales-support-contributors) of the repo.
+ `session_key`: String field, set the Cookie key to save the session.
+ `cookie_secret`: String field, set the secret passphrase te encrypt the session, *IMPORTANT:* Is better change this prop with a random key refering to [Key Generator](../tools/keyGenerator.html).
+ `[config.files]`
	- `temp_folder`: String filed, Set a temporary folder to create the cache files of PDF and Excel.

## `[database]`
This namespace set all data of databases connections.

+ `on_error_reconnect`: Bool filed, If is `true` when the database has a error force to reconnect.
+ `[database.postgres]`
	- `database`: String field, name of the database
	- `host`: String field, host of the database
	- `port`: Integer field, port of the database
	- `user`: String field, user to access the database
	- `password`: String field, password of the user
+ `[database.redis]`
	- `host`: String field, host of the database
	- `port`: Integer field, port of the database
	- `db`: Integer field, number of the database
	- `password`: String field, password of the database, *NOTE:* if the server not have a password set the password as empty string(`''`).
