---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: Bio-Security Form for Scout Activities and Camps
actionText: Quick Installation →
actionLink: /install/
features:
- title: User Friendly
  details: Very easy to use with intuitive interfaces
- title: Powerful
  details: With fast and powerful technologies
- title: Automated
  details: With secure verifications fail safe
footer: Made with love for all scout community
---
