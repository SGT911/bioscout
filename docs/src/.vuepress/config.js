const { description } = require('../../package')

module.exports = {
	/**
	* Ref：https://v1.vuepress.vuejs.org/config/#title
	*/
	title: 'Bio Scout - Documentation',
	/**
	* Ref：https://v1.vuepress.vuejs.org/config/#description
	*/
	description: description,
	dest: '../dist',
	/**
	* Extra tags to be injected to the page HTML `<head>`
	*
	* Ref：https://v1.vuepress.vuejs.org/config/#head
	*/
	head: [
		['meta', { name: 'theme-color', content: '#3eaf7c' }],
		['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
		['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
	],

	/**
	* Theme configuration, here is the default theme configuration for VuePress.
	*
	* ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
	*/
	themeConfig: {
		repo: 'https://gitlab.com/SGT911/bioscout',
		editLinks: false,
		docsDir: '',
		editLinkText: '',
		lastUpdated: false,
		nav: [
			{
				text: 'Configuration File',
				link: '/config/'
			}
		],
		sidebar: {
			'/install/': [
				{
					title: 'Pre Install',
					collapsable: false,
					children: [
						'pre',
						'config'
					]
				},
				{
					title: 'Installation',
					collapsable: false,
					children: ['install']
				}
			]
		}
	},

	/**
	* Apply plugins，ref：https://v1.vuepress.vuejs.org/en/plugin/
	*/
	plugins: [
		'@vuepress/plugin-back-to-top',
		'@vuepress/active-header-links',
		'@vuepress/nprogress',
	]
}
