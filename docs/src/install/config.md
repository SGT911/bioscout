# Install Configuration
***Bio Scout*** to install has a wizard installer script that configure the Install script and other properties to do a easy install.

This script creates a rootless directory where has all distribution folders, services, configuration and environment files.

The configure script have a `--help` command to see the flags details.
```bash
### Example ###
./configure.sh --help
# usage: ./configure.sh [--service-mode single|multiple|none] [--python-venv 0|1] [--python-path PYTHON_EXEC]
#        ./configure.sh -h|--help
# 
# 	{{{ Service Installation }}}
# 		--service-mode	-> Install service mode for single or mutiple instances, none for no install service on systemd
# 	{{{ Python Interpreter Configuration }}}
# 		--python-venv		-> Installation mode with or without virtual environment, by default (0)
# 		--python-path		-> Set default python interpreter path, by default "/usr/bin/python"
```

## Flags details

### `--service-mode`
+ `single`: Create a systemd service file to run a single instance ***Bio Scout*** installation.
+ `multiple`: Create a systemd service file to run a multiples instances ***Bio Scout*** installation, indicating the configuration file.
+ `none`: Skip the systemd service file creation.

### `--python-venv`
Set if ***Bio Scout*** will be run with a virtualenv installation, Set with `0 -> False`, `1 -> True`.

### `--python-path`
Set a custom python instance, if is not setted will be run `which python` to get the default python instance.

**NOTE:** If the default python installation is `3.8 or below`, is needed set a python `3.8 or above` installation.

## Common Examples
+ For dedicated server:
```bash
./configure.sh --service-mode single
```
+ Shared Server
```bash
./configure.sh --service-mode multiple \
	--python-path /usr/bin/python3.8
```
+ V-System
```
./configure.sh --service-mode none
```
+ AIO Server
```
./configure.sh --service-mode multiple \
	--python-venv 1 --python-path /usr/local/bin/python3.8
```
