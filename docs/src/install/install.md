# Installation
This is the easiest step only run the script `./install.sh` as sudo or with root privilegies.

```bash
sudo ./install.sh
# Or
su -c './install.sh'
```

And ***Bio Scout*** is succefuly installed, only missing configure the instance: yoa can refer to [Configuration](../config/) to get all ready to run the app.
