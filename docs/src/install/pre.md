# Pre Requisites
***Bio Scout*** is a final user application and use some services and libraries as **PostgreSQL** or **Tornado**, in this section will be show the step to install or upgrade the app requisites.

**NOTE:** The pre requisites installation has the step for [Ubuntu](https://ubuntu.com), [Debian](https://www.debian.org/), [CentOS](https://centos.org/), [Fedora Linux](https://getfedora.org/).

## Database
***Bio Scout*** use two database to work, **PostgreSQL** how works as the pricinpal database & **RedisDB** how works as a session manager using `UUID` as session IDs.

### Ubuntu & Debian
To install PostgreSQL on Debian & Ubuntu is recomended use a third-party repository owned by PostgreSQL and Redis will by installed from official repository.
```bash
### Run as root or with sudo ###
# Update & Upgrade system
apt update && apt upgrade -y

# Add PostgreSQL Repo
apt -y install gnupg2 wget lsb-core
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | tee /etc/apt/sources.list.d/pgdg.list

# Installing Databases
postgres_version=13
apt update && apt install -y \
	postgresql-$postgres_version postgresql-client-$postgres_version \
	redis
```
To configure can refer to respectively Documentation to configure and secure the databases.

### CentOS
To install PostgreSQL on Debian & Ubuntu is recomended use a third-party repository owned by PostgreSQL and Redis will by installed from the Remi's repository.
```bash
### Run as root or with sudo ###
# Make cache & Update system
yum makecache && yum update -y

# Add This-parties Repos
postgres_repo=https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
remi_repo=https://rpms.remirepo.net/enterprise/remi-release-8.rpm
yum install -y $postgres_repo $remi_repo && \
	yum makecache && yum module disable -y postgresql

# Install Databases
postgres_version=13
redis_version=6.0
yum install -y postgresql${postgres_version} postgresql${postgres_version}-server \
	postgresql${postgres_version}-libs postgresql${postgres_version}-devel && \
	yum module install -y redis:remi-${redis_version}

## Install PostgresDB
su postgres -c "/usr/pgsql-${postgres_version}/bin/initdb -D /var/lib/pgsql/data" # -W to set admin password directly
```
To configure can refer to respectively Documentation to configure and secure the databases.

### Fedora Linux
To install PostgreSQL on Debian & Ubuntu is recomended use a third-party repository owned by PostgreSQL and Redis will by installed from the Remi's repository.
```bash
### Run as root or with sudo ###
# Make cache & Update system
yum makecache && yum update -y

# Add This-parties Repos
postgres_repo=https://download.postgresql.org/pub/repos/yum/reporpms/F-32-x86_64/pgdg-fedora-repo-latest.noarch.rpm
remi_repo=https://rpms.remirepo.net/fedora/remi-release-32.rpm
yum install -y $postgres_repo $remi_repo && \
	yum makecache && yum module disable -y postgresql

# Install Databases
postgres_version=13
yum install --enablerepo=remi -y postgresql${postgres_version} postgresql${postgres_version}-server \
	postgresql${postgres_version}-libs postgresql${postgres_version}-devel redis

## Install PostgresDB
su postgres -c "/usr/pgsql-${postgres_version}/bin/initdb -D /var/lib/pgsql/data" # -W to set admin password directly
```
To configure can refer to respectively Documentation to configure and secure the databases.

## Interpreter
***Bio Scout*** is written in [Python](https://www.python.org/) in the version 3.8 or above.

### Debian & Ubuntu
```bash
### Run as root or with sudo ###
# Update & Upgrade system
apt update && apt upgrade -y

# Install build dependencies
apt install build-essential zlib1g-dev \
	libncurses5-dev libgdbm-dev libnss3-dev \
	libssl-dev libsqlite3-dev libreadline-dev \
	libffi-dev libbz2-dev wget curl

# Build & Install interpreter
py_version=3.8.7
wget https://www.python.org/ftp/python/${py_version}/Python-${py_version}.tgz
tar xzf Python-${py_version}.tgz && cd Python-${py_version}
./configure --enable-optimizations
make && make altinstall

# [Optional] Change as default version
py_major=3.8
ln -sf /usr/local/bin/python${py_major} /usr/bin/python
ln -sf /usr/local/bin/pip${py_major} /usr/bin/pip

# [Optional] Install virtualenv
pip install --no-cache-dir virtualenv
```

### CentOS & Fedora Linux
```bash
### Run as root or with sudo ###
# Make cache & Update system
yum makecache && yum update -y

# Install interpreter
yum module -y install python38 python38-devel python38-pip

# [Optional] Install virtualenv
pip install --no-cache-dir virtualenv
```

# *[Optional]* HTTP Proxy
***Bio Scout*** can expose the app directly, but is most secure include a Proxy for secure the app and optimize the performance with Cache and gZIP.

In this case will be use [Nginx](http://nginx.org/).

### Debian & Ubuntu
```bash
### Run as root or with sudo ###
# Update & upgrade the system
apt update && apt upgrade -y

# Install nginx
apt install nginx -y
```

### CentOS & Fedora Linux
```bash
### Run as root or with sudo ###
# Make cache & Update system
yum makecache && yum update -y

# Install nginx
yum install -y nginx
```
