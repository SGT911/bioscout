#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- author: sgt911 -*-

from tornado.httpserver import HTTPServer
from tornado.web import Application, StaticFileHandler
from tornado.ioloop import IOLoop
from tornado.netutil import bind_sockets as bind_tcp, bind_unix_socket as bind_unix
from tornado.process import fork_processes

from config import DATA as ENV

from os import getcwd
from os.path import join as path_join
from utils import make_templates

from utils.controllers import NotFoundHandler, CDNHandler, SingleCDNHandler, SimpleRenderHandler
from routes import IndexHandler
from routes.install import InstallSecondStepHandler
from routes.login import LoginHandler, LogoutHandler
from routes.admins import AdminChangePasswordHandler, AdminCreateHandler
from routes.admins.events import CreateEventHandler, GetAllEventsHandler, GetOneEventHandler
from routes.admins.events.temps import TemperatureEventHandler
from routes.admins.subjects import GetAllSubjectsHandler, GetOneSubjectsHandler
from routes.admins.symptoms import SymptomsHandler
from routes.admins.ni_types import NiTypesHandler
from routes.signup import SignUpHandler, SignUpSymptomsHandler
from routes.api.install import APIFirstStepInstallHandler, APISecondStepInstallHandler
from routes.api.events import APIEventsHandler
from routes.api.subjects import APISubjectHandler, APISubjectParentHandler
from routes.api.signup import APISignUpSymptoms, APISignUpTemperatures
from routes.api.admins import APIChangePasswordHandler
from routes.files.events import EventSummaryHandler
from routes.files.signup import ParentAuthPDFHandler
from routes.files.subjects import SubjectsSummaryHandler


def make_routes() -> list:
	loader = make_templates()

	return [
		# CDN
		(r'/static/cdn/bulma/(.+)', CDNHandler, dict(prefix='https://cdn.jsdelivr.net/npm/bulma@0.9.1/css')),
		(r'/static/cdn/vuex/(.+)', CDNHandler, dict(prefix='https://cdn.jsdelivr.net/npm/vuex@3.6.0/dist')),
		(r'/static/cdn/vue/(.+)', CDNHandler, dict(prefix='https://cdn.jsdelivr.net/npm/vue@2.6.12/dist')),
		(r'/static/cdn/materialIcons\.css', SingleCDNHandler, dict(
			source='https://fonts.googleapis.com/css?family=Material+Icons',
		)),

		# Static
		(r'/static/(.+)', StaticFileHandler, dict(path=path_join(getcwd(), 'static'))),

		# API
		(r'/api/install/first', APIFirstStepInstallHandler),
		(r'/api/install/second', APISecondStepInstallHandler),
		(r'/api/events', APIEventsHandler),
		(r'/api/subjects/([0-9]+)', APISubjectHandler),
		(r'/api/subjects/parent/([0-9]+)', APISubjectParentHandler),
		(r'/api/signup/symptoms', APISignUpSymptoms),
		(r'/api/signup/temperatures', APISignUpTemperatures),
		(r'/api/user/changePassword', APIChangePasswordHandler),

		# Computed Files
		(r'/files/events/event_([0-9]+)\.xlsx', EventSummaryHandler),
		(r'/files/auth/([0-9]+)\.pdf', ParentAuthPDFHandler),
		(r'/files/subjects\.xlsx', SubjectsSummaryHandler),

		# Routes
		## Credits
		(r'/credits', SimpleRenderHandler, dict(loader=loader, template='credits.jinja')),
		## Installation Wizard
		(r'/install', SimpleRenderHandler, dict(loader=loader, template='install/info.jinja')),
		(r'/install/step/first', SimpleRenderHandler, dict(loader=loader, template='install/first.jinja')),
		(r'/install/step/second', InstallSecondStepHandler, dict(loader=loader)),
		## Administration Routes
		(r'/', IndexHandler, dict(loader=loader)),
		(r'/login', LoginHandler, dict(loader=loader)),
		(r'/logout', LogoutHandler),
		(r'/events', GetAllEventsHandler, dict(loader=loader)),
		(r'/events/create', CreateEventHandler, dict(loader=loader)),
		(r'/events/([0-9]+)', GetOneEventHandler, dict(loader=loader)),
		(r'/events/([0-9]+)/temperature', TemperatureEventHandler, dict(loader=loader)),
		(r'/subjects', GetAllSubjectsHandler, dict(loader=loader)),
		(r'/subjects/([0-9]+)', GetOneSubjectsHandler, dict(loader=loader)),
		(r'/user/changePassword', AdminChangePasswordHandler, dict(loader=loader)),
		(r'/admin/symptoms', SymptomsHandler, dict(loader=loader)),
		(r'/admin/niTypes', NiTypesHandler, dict(loader=loader)),
		(r'/admin/create', AdminCreateHandler, dict(loader=loader)),
		## Signup Routes
		(r'/signup/([0-9]+)-([0-9]+)', SignUpHandler, dict(loader=loader)),
		(r'/signup/symptoms/([0-9]+)-([0-9]+)', SignUpSymptomsHandler, dict(loader=loader)),
	]


def make_app() -> Application:
	return Application(
		make_routes(),
		default_handler_class=NotFoundHandler,
		cookie_secret=ENV['config']['cookie_secret'],
	)


if __name__ == '__main__':
	fork_processes(1)

	app = make_app()
	server, sockets = HTTPServer(app), list()

	if ENV['server']['web']['active']:
		sockets.append(bind_tcp(ENV['server']['web']['port'], ENV['server']['web']['host']))

	if ENV['server']['unixsocket']['active']:
		sockets.append([bind_unix(ENV['server']['unixsocket']['file'], int(str(ENV['server']['unixsocket']['mode']), base=8))])

	for socket in sockets:
		server.add_sockets(socket)

	IOLoop.current().start()
