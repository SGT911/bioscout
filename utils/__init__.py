from abc import ABC

from tornado.template import Loader
from tornado.web import RequestHandler

from typing import Any, Optional, List, Union

from os import getcwd
from os.path import join as path_join

import json
import hashlib


def make_templates() -> Loader:
	return Loader(path_join(getcwd(), "views"))


def hash_password(passwd: str) -> str:
	return hashlib.sha512(passwd.encode()).hexdigest()


class AttrDict(dict):
	def __setattr__(self, key: str, value: Any):
		self[key] = value

	def __getattr__(self, key: str) -> Any:
		if key in self:
			return self[key]
		else:
			raise AttributeError(f"The attribute \"{key}\" does not exist")

	def __delattr__(self, key: str):
		if key in self:
			del self[key]
		else:
			raise AttributeError(f"The attribute \"{key}\" does not exist")

	def __repr__(self):
		cls_name = self.__class__.__name__
		data = list()
		for k, v in self.items():
			data.append('%s=%r' % (k, v))

		return f"{cls_name}({', '.join(data)})"


class APIError(AttrDict):
	def __init__(self, name: str = "Error", description: Optional[Any] = None):
		self.name = name
		self.description = description

	@staticmethod
	def parse_exception(ex: Exception) -> AttrDict:
		if len(ex.args) == 0:
			desc = None
		elif len(ex.args) == 1:
			desc = ex.args[0]
		else:
			desc = ex.args

		return APIError(
			name=ex.__class__.__name__,
			description=desc,
		)


class APIResponse(AttrDict):
	def __init__(self, status: str = "ok", payload: Optional[Any] = None, errors: Optional[List[Union[APIError, AttrDict]]] = None):
		if errors is None:
			errors = list()

		self.status = status
		self.payload = payload
		self.errors = errors


class APIRequestHandler(RequestHandler, ABC):
	def parse_data(self):
		if self.request.headers.get('Content-Type').startswith('application/json'):
			try:
				parsed = json.loads(self.request.body)
			except json.JSONDecodeError:
				return self.write_response(400, APIResponse(
					status='error',
					errors=[
						APIError(
							name='ParseError',
							description='Error parsing the body',
						),
					],
				))
			else:
				if isinstance(parsed, dict):
					if 'payload' in parsed and len(parsed) == 1:
						return parsed['payload']
					else:
						return parsed
				else:
					return self.write_response(400, APIResponse(
						status='error',
						errors=[
							APIError(
								name='ParseError',
								description='Parse only accept objects, try using \"{\"payload\": <send_payload>}\"',
							),
						],
					))
		else:
			description = 'The data is only parsable if is sent in JSON with \"application/json\" value in Content-Type header'
			return self.write_response(400, APIResponse(
				status='error',
				errors=[
					APIError(
						name='ParseError',
						description=description
					),
				],
			))

	def write_response(self, code: int, message: Union[APIResponse, Any]):
		self.set_status(code)
		self.write(json.dumps(message, indent='\t'))
		return self.finish()
