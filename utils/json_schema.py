from jsonschema import Draft7Validator
from typing import List, Any


class ValidationError(Exception):
	pass


def validate(instance: Any, schema: dict) -> List[ValidationError]:
	validator = Draft7Validator(schema)
	errors = validator.iter_errors(instance)
	
	return list(map(lambda err: ValidationError(dict(
		msg=err.message,
		path='root.' + '.'.join(err.path),
		instance=err.instance,
	)), errors))
