from tornado.web import RequestHandler
from tornado.template import Loader
from aiohttp import ClientSession
from utils import make_templates

from abc import ABC
from typing import Optional


class NotFoundHandler(RequestHandler, ABC):
	def prepare(self):
		uri = self.request.uri
		loader = make_templates()

		template = loader.load('errors/404.jinja').generate(uri=uri)

		self.set_status(404)
		self.finish(template)


class ForbiddenHandler(RequestHandler, ABC):
	def prepare(self, reason: Optional[str] = None):
		loader = make_templates()

		template = loader.load('errors/403.jinja').generate(reason=reason)

		self.set_status(403)
		self.finish(template)


class CDNHandler(RequestHandler, ABC):
	cache = dict()

	def initialize(self, prefix: str):
		self.prefix = prefix

	async def get(self, *args):
		uri = args[0]

		if uri not in self.cache:
			async with ClientSession() as session:
				data = await session.get(f'{self.prefix}/{uri}')

				if data.status == 404:
					return NotFoundHandler.prepare(self)

				self.cache[uri] = dict(
					type = data.headers['content-type'],
					data = await data.text(),
				)

		data = self.cache[uri]

		self.set_header('Content-Type', data['type'])		
		return self.finish(data['data'])


class SingleCDNHandler(RequestHandler, ABC):
	cache: Optional[dict] = None

	def initialize(self, source: str):
		self.source = source

	async def get(self):
		if self.cache == None:
			async with ClientSession() as session:
				data = await session.get(self.source)

				self.cache = dict(
					type = data.headers['content-type'],
					status = data.status,
					data = await data.text(),
				)

		self.set_header('Content-Type', self.cache['type'])
		self.set_status(self.cache['status'])
		return self.finish(self.cache['data'])


class SimpleRenderHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader, template: str):
		self.loader = loader
		self.template = template

	def get(self):
		template = self.loader.load(self.template).generate()
		return self.finish(template)