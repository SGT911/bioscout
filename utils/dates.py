from datetime import date, datetime, timedelta
import time
import re

from typing import Optional

DATETIME_REGEX = r'^([0-9]{4})-([0-1][0-9])-([0-3][0-9])T([0-2][0-9]):([0-6][0-9]):([0-9]{2})(Z|[+-][0-9]{4})$'
TZ_REGEX = r'^Z|[+-][0-9]{4}$'

weekdays = [
	'Lunes',
	'Martes',
	'Miercoles',
	'Jueves',
	'Viernes',
	'Sabado',
	'Domingo',
]

months = [
	'Null',
	'Enero',
	'Febrero',
	'Marzo',
	'Abril',
	'Mayo',
	'Junio',
	'Julio',
	'Agosto',
	'Septiembre',
	'Octubre',
	'Noviembre',
	'Diciembre'
]


def get_tz_offset() -> timedelta:
	return parse_tz(int(time.strftime('%z')))


def parse_isodate(date: str) -> Optional[datetime]:
	match = re.findall(
		DATETIME_REGEX,
		date
	)
	if len(match) == 0:
		return None

	match = match[0]

	tz = 0
	if match[6] != 'Z':
		tz = int(match[6])

	return parse_date(*list(map(lambda x: int(x), match[:6]))) + parse_tz(tz)


def to_isodate(date: datetime) -> str:
	return date.isoformat() + 'Z'


def parse_date(
		year: int,
		month: int,
		day: int,
		hour: int = 0,
		minute: int = 0,
		second: int = 0
	) -> datetime:
	return datetime(
		year=year,
		month=month,
		day=day,
		hour=hour,
		minute=minute,
		second=second
	)


def parse_tz(tz: int = 0) -> timedelta:
	return timedelta(hours=(tz / 100))


def create_delta(
		seconds: int = 0,
		minutes: int = 0,
		hours: int = 0,
		days: int = 0,
		months: int = 0
	) -> timedelta:
	return timedelta(
		seconds=seconds,
		minutes=minutes,
		hours=hours,
		days=days + (months * 30)
	)


def now() -> datetime:
	today = datetime.now()

	return parse_date(
		year = today.year,
		month = today.month,
		day = today.day,
	)


def get_years(from_date: date) -> int:
	diff = date.today() - from_date

	diff_days = diff.total_seconds() / (60 * 60 * 24)

	return int(diff_days / 365)