from xlsxwriter import Workbook
from io import FileIO

from os import getcwd
from os.path import join as path_join

from config import DATA as ENV

import string
import random

from typing import Any, Tuple, Callable


def create_filename(l: int = 20):
	filename = str()
	for _ in range(l):
		filename += random.choice(string.ascii_lowercase)
	return filename


def get_filename(ext: str) -> str:
	return f"{ENV['config']['files']['temp_folder']}/{create_filename()}.{ext}"


def create_excel(filename: str) -> Workbook:
	return Workbook(filename, { 'in_memory': True })


def get_tex_template() -> str:
	file = FileIO(path_join(getcwd(), 'locale', 'pdf', f"{ENV['config']['locale']}.tex"), mode='r')
	data = file.read().decode()
	file.close()
	
	return data


class AutoHandledFile:
	def __init__(self, filename: Any, handler: Callable[[str], Any]):
		self.filename = filename
		self.handler = handler

	def get_file_content(self) -> bytes:
		file = FileIO(self.filename, mode='rb')
		data = file.read()
		file.close()

		return data

	def __enter__(self, *args) -> Tuple[Any, Callable[[], bytes]]:
		self.file = self.handler(self.filename)

		return self.file, self.get_file_content

	def __exit__(self, *args):
		pass
