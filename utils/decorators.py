from typing import Callable


def run_after(post: Callable) -> Callable:
	def decorator(f: Callable):
		def wrapper(*args, **kwargs):
			ret_val = f(*args, **kwargs)
			post(*args, **kwargs)
			return ret_val
		return wrapper

	return decorator
