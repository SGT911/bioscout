from datetime import date
from utils.dates import weekdays, months

from typing import Any

from config import DATA as env


def get_lang() -> str:
	return env['config']['lang']


def script(url: str) -> str:
	return f'<script defer async type="module" src="{url}"></script>'


def vue(data: str) -> str:
	return '{{ %s }}' % (data, )


def format_date(raw_date: date) -> str:
	return f'{weekdays[raw_date.weekday()]} {raw_date.day} de {months[raw_date.month]} de {raw_date.year}'


def third_operator(condition: bool, on_yes: Any, on_no: Any) -> Any:
	if condition:
		return on_yes

	return on_no
