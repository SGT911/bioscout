from session.connection import get_client
from session.id import create_id, check_id

from utils.decorators import run_after
from utils.dates import get_tz_offset
from datetime import datetime, timedelta

from typing import Any, Iterable, Tuple

from config import DATA as ENV

EXPIRATION_TIME = 60 * 30  # Expiration time in seconds
COOKIE_KEY = ENV['config']['session_key'] # Cookie Of the Session


class Session(object):
	def __init__(self, session_id: str, force_create: bool = False):
		self.redis = get_client()
		self.session = session_id

		if self.exists():
			self._reload()
		elif force_create:
			self.create()

	def destroy(self):
		self.redis.delete(self.session)

	def _reload(self, *args, **kwargs):
		self.redis.expire(self.session, EXPIRATION_TIME)

	@run_after(_reload)
	def exists(self) -> bool:
		return bool(self.redis.exists(self.session))

	@run_after(_reload)
	def key_exist(self, key: str) -> bool:
		try:
			_ = self.__getitem__(key)
			return True
		except Exception:
			return False

	@run_after(_reload)
	def create(self):
		self.redis.hset(self.session, '_', '_')

	@run_after(_reload)
	def __setitem__(self, key: str, value: Any):
		self.redis.hset(self.session,  key, value)

	@run_after(_reload)
	def __getitem__(self, key: str) -> Any:
		value = self.redis.hget(self.session, key)
		if value is None:
			raise KeyError(f'The key "{key}" does not exist')

		return value.decode()

	@run_after(_reload)
	def __delitem__(self, key: str):
		if not self.redis.hdel(self.session, key):
			raise KeyError(f'The key "{key}" does not exist')

	@run_after(_reload)
	def __iter__(self) -> Iterable[Tuple[str, Any]]:
		for k in self.redis.hkeys():
			if k == '_':
				continue

			yield k, self.redis.hget(self.session, k)


def session(f):
	def wrapper(self, *args, **kwargs):
		session_id = self.get_secure_cookie(COOKIE_KEY, None)
		if session_id is not None:
			session_id = session_id.decode()

		if session_id is not None and check_id(session_id):
			req_session = Session(session_id)
			if not req_session.exists():
				session_id = create_id()
				req_session = Session(session_id, True)
		else:
			session_id = create_id()
			req_session = Session(session_id, True)

		expires = (datetime.now() - get_tz_offset()) + timedelta(seconds=EXPIRATION_TIME)
		self.set_secure_cookie(COOKIE_KEY, session_id, expires=expires)

		return f(self, req_session, *args, **kwargs)

	return wrapper
