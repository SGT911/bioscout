from redis import Redis as Client
from config import DATA as ENV

from typing import Optional

if ENV['database']['redis']['password'] == '':
	ENV['database']['redis']['password'] = None

REDIS: Optional[Client] = None


def get_client() -> Client:
	global REDIS
	if REDIS is None:
		REDIS = Client(**ENV['database']['redis'])

	return REDIS
