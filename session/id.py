from uuid import uuid4, UUID


def create_id() -> str:
	return str(uuid4())


def check_id(uuid: str) -> bool:
	try:
		uuid = UUID(uuid)
	except Exception:
		return False
	else:
		return True
