-------------------------
-- Common Data (es_CO) --
-------------------------

INSERT INTO ni_types (name, abbreviation)
	VALUES  ('cedula de ciudadania',  'CC'),
			('tarjeta de identidad',  'TI'),
			('cedula de extranjeria', 'CE');

INSERT INTO possibles_symptoms (description)
	VALUES  ('fiebre'),
			('tos'),
			('dolor de garganta'),
			('malestar general'),
			('dolor de cabeza'),
			('diarrea'),
			('dolor abdominal'),
			('nauseas'),
			('vomito'),
			('dolor en el cuerpo'),
			('perdida del olfato o gusto'),
			('dificultad para respirar'),
			('contacto con alguien positivo de covid-19'),
			('viaje al exterior del pais en los ultimos 14 dias');