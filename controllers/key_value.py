from database import get_client, AutoHandledCursor
from typing import Optional, Any


def get_value(key: str) -> Optional[Any]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""SELECT value FROM key_value WHERE key = %s""", (key, ))
		if cur.rowcount == 0:
			return None

		return cur.fetchone()['value']


def set_value(key: str, value: Any):
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""INSERT INTO key_value VALUES (%s, %s)""", (key, value))

			get_client().commit()
		except Exception:
			get_client().rollback()
