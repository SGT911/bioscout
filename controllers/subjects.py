from database import get_client, AutoHandledCursor
from utils import AttrDict
from typing import List, Optional, Any

subject_schema = {
	'type': 'object',
	'required': [
		'ni_type',
		'ni',
		'full_name',
		'birthday',
		'email',
		'phone',
		'fork',
	],
	'additionalProperties': False,
	'properties': {
		'ni_type': {
			'type': 'integer',
			'exclisiveMinimum': 0,
		},
		'ni': {
			'type': 'integer',
			'exclisiveMinimum': 99999,
		},
		'full_name': {
			'type': 'string',
		},
		'birthday': {
			'type': 'string',
			'pattern': r'^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
		},
		'email': {
			'type': 'string',
		},
		'phone': {
			'type': 'integer',
		},
		'fork': {
			'type': 'integer',
			'exclisiveMinimum': 0,
		},
		'parent': {
			'type': 'object',
			'required': [
				'ni_type',
				'ni',
				'full_name',
				'email',
				'phone',
			],
			'additionalProperties': False,
			'properties': {
				'ni_type': {
					'type': 'integer',
					'exclisiveMinimum': 0,
				},
				'ni': {
					'type': 'integer',
					'exclisiveMinimum': 99999,
				},
				'full_name': {
					'type': 'string',
				},
				'email': {
					'type': 'string',
				},
				'phone': {
					'type': 'integer',
				},
			}
		},
	},
}

update_subject_schema = subject_schema.copy()
update_subject_schema['required'] = ['ni']
update_subject_schema['properties']['parent']['required'] = ['ni']


def get_ni_types() -> List[AttrDict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT id, name, abbreviation FROM ni_types
		""")

		return list(map(lambda x: AttrDict(x), cur))


def add_ni_type(abbreviation: str, name: str):
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				INSERT INTO ni_types (abbreviation, name)
					VALUES (%s, %s)
			""", (abbreviation, name))

			get_client().commit()
		except Exception as e:
			get_client().rollback()
			raise e

def get_forks() -> List[AttrDict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT id, fork, name FROM forks ORDER BY id ASC
		""")

		return list(map(lambda x: AttrDict(x), cur))


def get_from_ni(ni: int) -> Optional[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				*
			FROM v_subjects
				WHERE ni = %s
				LIMIT 1
		""", (ni, ))

		if cur.rowcount == 0:
			return None

		return cur.fetchone()


def get_all_subjects() -> List[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				*
			FROM v_subjects
		""")

		return list(iter(cur))


def get_person_from_ni(ni: int) -> Optional[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				pe.ni,
				('{' ||
					'"id": '           || nt.id          || ', '  ||
					'"name": "'        || nt.name        || '", ' ||
					'"abbreviation": "' || nt.abbreviation || '" '  || 
				'}')::JSON AS ni_type,
				pe.full_name,
				pe.email,
				pe.phone
			FROM people AS pe
				INNER JOIN ni_types AS nt
					ON nt.id = pe.ni_type
				WHERE ni = %s
				LIMIT 1
		""", (ni, ))

		if cur.rowcount == 0:
			return None

		return cur.fetchone()


def create_people(data: dict):
	with AutoHandledCursor(get_client()) as cur:
		try:
			data['full_name'] = data['full_name'].lower()

			cur.execute("""
				INSERT INTO people
					VALUES (%(ni)s, %(ni_type)s, %(full_name)s, %(email)s, %(phone)s)
			""", data)
			get_client().commit()
			return True
		except Exception as e:
			get_client().rollback()
			raise e


def create_subject(data: dict):
	with AutoHandledCursor(get_client()) as cur:
		try:
			if 'parent' in data:
				parent = data['parent']
				del data['parent']
				if get_person_from_ni(parent['ni']) is None:
					create_people(parent)
				else:
					parent_ni = parent['ni']
					parent_data = parent
					parent = get_person_from_ni(parent_ni)

					for k in iter(['ni_type', 'full_name', 'email', 'phone']):
						if k in parent_data and parent_data[k] != parent[k]:
							update_person(parent_ni, k, parent_data[k])

				data['parent'] = parent['ni']
			else:
				data['parent'] = None

			data['full_name'] = data['full_name'].lower()

			if get_person_from_ni(data['ni']) is None:
				cur.execute("""
					INSERT INTO people
						VALUES (%(ni)s, %(ni_type)s, %(full_name)s, %(email)s, %(phone)s)
				""", data)
			else:
				subject_ni = data['ni']
				subject = get_person_from_ni(subject_ni)

				for k in iter(['ni_type', 'full_name', 'email', 'phone']):
					if k in data and data[k] != subject[k]:
						update_person(subject_ni, k, data[k])

			cur.execute("""
				INSERT INTO subjects
					VALUES (%(ni)s, %(fork)s, %(birthday)s::DATE, %(parent)s)
			""", data)

			get_client().commit()
		except Exception as e:
			get_client().rollback()
			raise e


def update_person(ni: int, field: str, data: Any):
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				UPDATE people SET {field} = %s WHERE ni = %s
			""".format(field=field), (data, ni))

			get_client().commit()
		except Exception as e:
			get_client().rollback()
			raise e


def update_subject(ni: int, field: str, data: Any):
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				UPDATE subjects SET {field} = %s WHERE ni = %s
			""".format(field=field), (data, ni))

			get_client().commit()
		except Exception as e:
			get_client().rollback()
			raise e
