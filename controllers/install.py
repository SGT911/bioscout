from database import get_client, AutoHandledCursor
from controllers import is_installed
from utils import hash_password
from json import dumps as json_dump

first_step = {
	'type': 'object',
	'required': [
		'group_number',
		'group_name',
		'forks',
	],
	'additionalProperties': False,
	'properties': {
		'group_number': {
			'type': 'integer',
			'exclisiveMinimum': 0,
		},
		'group_name': {
			'type': 'string',
		},
		'forks': {
			'type': 'array',
			'minItems': 5,
			'items': {
				'type': 'object',
				'required': [
					'type',
					'name',
				],
				'additionalProperties': False,
				'properties': {
					'type': {
						'type': 'string',
					},
					'name': {
						'type': 'string',
					},
				},
			},
		},
	},
}

second_step = {
	'type': 'object',
	'required': [
		'ni_type',
		'ni',
		'full_name',
		'birthday',
		'email',
		'phone',
		'fork',
		'password',
	],
	'additionalProperties': False,
	'properties': {
		'ni_type': {
			'type': 'integer',
			'exclisiveMinimum': 0,
		},
		'ni': {
			'type': 'integer',
			'exclisiveMinimum': 99999,
		},
		'full_name': {
			'type': 'string',
		},
		'birthday': {
			'type': 'string',
			'pattern': r'^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
		},
		'email': {
			'type': 'string',
		},
		'phone': {
			'type': 'integer',
		},
		'fork': {
			'type': 'integer',
			'exclisiveMinimum': 0,
		},
		'password': {
			'type': 'string',
		},
	},
}


def run_first_step(data: dict) -> bool:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT COUNT(id) FROM forks
		""")

		row = cur.fetchone()
		if row['count'] > 0:
			return False

	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				INSERT INTO key_value VALUES (%s, %s), (%s, %s)
			""", (
				'group_number',
				json_dump(data['group_number']),
				'group_name',
				json_dump(data['group_name'].lower())
			))

			forks = list(map(lambda x: {
				'type': str(x['type']).lower(),
				'name': str(x['name']).lower(),
			}, data['forks']))

			cur.executemany("""
				INSERT INTO forks (fork, name) VALUES (%(type)s, %(name)s)
			""", forks)

			get_client().commit()
			return True
		except Exception as e:
			get_client().rollback()
			raise e


def run_second_step(data: dict) -> bool:
	if is_installed():
		return False

	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				INSERT INTO people VALUES (
					%s,
					%s,
					%s,
					%s,
					%s
				)
			""", (
				data['ni'],
				data['ni_type'],
				data['full_name'].lower(),
				data['email'],
				data['phone'],
			))

			cur.execute("""
				INSERT INTO subjects (ni, fork, birthday) VALUES (
					%s,
					%s,
					%s::DATE
				)
			""", (
				data['ni'],
				data['fork'],
				data['birthday'],
			))

			cur.execute("""
				INSERT INTO admins (subject, password) VALUES (%s, %s)
			""", (
				data['ni'],
				hash_password(data['password'])
			))

			get_client().commit()
			return True
		except Exception as e:
			get_client().rollback()
			raise e
