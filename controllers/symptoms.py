from database import get_client, AutoHandledCursor
from typing import List


def get_possible_symptoms() -> List[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT id, description FROM possibles_symptoms
		""")

		return [row for row in cur]


def add_symptom(description: str):
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				INSERT INTO possibles_symptoms (description)
					VALUES (%s)
			""", (description, ))

			get_client().commit()
		except Exception as e:
			get_client().rollback()
			raise e