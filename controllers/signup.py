from database import get_client, AutoHandledCursor

symptoms_schema = {
	'type': 'object',
	'required': [
		'ni',
		'evt',
		'symptoms',
	],
	'additionalProperties': False,
	'properties': {
		'ni': {
			'type': 'integer',
		},
		'evt': {
			'type': 'integer',
			'exclusiveMinimum': 0,
		},
		'other': {
			'type': 'string',
		},
		'symptoms': {
			'type': 'array',
			'items': {
				'type': 'integer',
				'exclusiveMinimum': 0,
			},
		},
	},
}

temperature_schema = {
	'type': 'object',
	'required': [
		'event',
		'subjects',
	],
	'additionalProperties': False,
	'properties': {
		'event': {
			'type': 'integer',
		},
		'subjects': {
			'type': 'object',
			'propertyNames': {
				'pattern': r'[0-9]+',
			},
			'additionalProperties': {
				'type': 'object',
				'required': [
					'initial',
					'final',
				],
				'additionalProperties': False,
				'properties': {
					'initial': {
						'type': 'number',
					},
					'final': {
						'type': ['number', 'null'],
					},
				},
			},
		},
	}
}


def signup_symptom(data: dict):
	if 'other' not in data:
		data['other'] = None

	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				INSERT INTO registered_subjects (event_id, subject, observations)
					VALUES (%(evt)s, %(ni)s, %(other)s)
				RETURNING id
			""", data)

			signup_id = cur.fetchone()['id']

			rows = [(signup_id, symptom) for symptom in data['symptoms']]

			cur.executemany("""
				INSERT INTO registered_subjects_symptoms
					VALUES (%s, %s)
			""", rows)

			get_client().commit()
		except Exception as e:
			get_client().rollback()
			raise e


def signup_temperature(ni: int, evt_id: int, initial: float, final: float):
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				INSERT INTO registered_temperature (subject, event_id, initial_temp, final_temp)
					VALUES (%s, %s, %s, %s)
			""", (ni, evt_id, initial, final))

			get_client().commit()
		except Exception as e:
			get_client().rollback()
			raise e

def update_temperature(ni: int, evt_id: int, initial: float, final: float):
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				UPDATE registered_temperature
					SET initial_temp=%s, final_temp=%s
					WHERE subject=%s AND event_id=%s
			""", (initial, final, ni, evt_id))

			get_client().commit()
		except Exception as e:
			get_client().rollback()
			raise e
