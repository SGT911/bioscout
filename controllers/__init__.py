from database import get_client, AutoHandledCursor


def db_is_installed() -> bool:
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""SELECT COUNT(key) FROM key_value""")
			return True
		except Exception:
			get_client().rollback()
			return False


def is_installed() -> bool:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""SELECT * FROM subjects""")

		return cur.rowcount != 0
