from database import get_client, AutoHandledCursor
from utils import hash_password

from typing import Optional, List

change_password_schema = {
	'type': 'object',
	'reqired': [
		'ni',
		'password',
	],
	'additionalProperties': False,
	'properties': {
		'ni': {
			'type': 'integer',
		},
		'password': {
			'type': 'string',
		},
	},
}


def login(ni: int, password: str) -> bool:
	password = hash_password(password)
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT COUNT(id) AS count
				FROM admins
				WHERE subject=%s AND password=%s
		""", (ni, password))

		count = cur.fetchone()['count']

		return count > 0


def get_admin(ni: int) -> Optional[int]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT id
				FROM admins
				WHERE subject=%s
		""", (ni, ))

		if cur.rowcount > 0:
			return cur.fetchone()['id']
		return None


def get_possible_admins() -> List[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				subj.*
			FROM v_subjects AS subj
			LEFT JOIN admins AS adm
				ON subj.ni = adm.subject
			WHERE adm.subject IS NULL AND (NOW()::DATE - subj.birthday) / 365.25 >= 18
		""")

		return list(iter(cur))


def create_admin(ni: int, password: str):
	password = hash_password(password)
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				INSERT INTO admins (subject, password)
					VALUES (%s, %s)
			""", (ni, password))

			get_client().commit()
		except Exception:
			get_client().rollback()


def change_password(ni: int, password: str):
	password = hash_password(password)
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				UPDATE admins
					SET password=%s
					WHERE subject=%s
			""", (password, ni))

			get_client().commit()
		except Exception:
			get_client().rollback()