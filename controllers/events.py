from database import get_client, AutoHandledCursor
from controllers.admins import get_admin
from typing import Optional, List, Union
from datetime import date

create_event_schema = {
	'type': 'object',
	'required': [
		'ni',
		'name',
		'date',
		'forks',
	],
	'additionalProperties': False,
	'properties': {
		'ni': {
			'type': 'integer',
		},
		'name': {
			'type': 'string',
		},
		'date': {
			'type': 'string',
			'pattern': r'^[0-9]{4}-[0-9]{2}-[0-9]{2}$',
		},
		'forks': {
			'type': 'array',
			'items': {
				'type': 'integer',
			},
		},
	},
}


def create_event(data: dict) -> int:
	with AutoHandledCursor(get_client()) as cur:
		try:
			cur.execute("""
				INSERT INTO events(creator, name, event_date)
					VALUES (%s, %s, %s::DATE)
				RETURNING id
			""", (get_admin(data['ni']), data['name'], data['date']))

			evt_id = cur.fetchone()['id']
			forks = [(evt_id, fork) for fork in data['forks']]

			cur.executemany("""
				INSERT INTO allowed_forks_event VALUES (%s, %s)
			""", forks)

			get_client().commit()
			return evt_id
		except Exception as e:
			get_client().rollback()
			raise e


def get_one_event(evt_id: int) -> Optional[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				evts.id,
				evts.creator,
				adm.subject AS creator_ni,
				sub.full_name AS creator_name,
				evts.name,
				evts.event_date,
				ARRAY_AGG(('{' ||
					'"id": '    || fo.id   || ', '  ||
					'"name": "' || fo.name || '", ' ||
					'"fork": "' || fo.fork || '" '  ||
				'}')::JSON) as forks
			FROM events AS evts
				INNER JOIN admins as adm
					ON evts.creator = adm.id
				INNER JOIN v_subjects AS sub
					ON sub.ni = adm.subject
				INNER JOIN allowed_forks_event AS fo_evt
					ON fo_evt.event_id = evts.id
				INNER JOIN forks AS fo
					ON fo.id = fo_evt.fork_id
				WHERE evts.id = %s
				GROUP BY evts.id, evts.creator, adm.subject, sub.full_name, evts.name, evts.event_date
		""", (evt_id,))

		if cur.rowcount > 0:
			return cur.fetchone()

		return None


def get_all_events() -> List[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				evts.id,
				evts.creator,
				adm.subject AS creator_ni,
				sub.full_name AS creator_name,
				evts.name,
				evts.event_date,
				ARRAY_AGG(('{' ||
					'"id": '    || fo.id   || ', '  ||
					'"name": "' || fo.name || '", ' ||
					'"fork": "' || fo.fork || '" '  ||
				'}')::JSON) as forks
			FROM events AS evts
				INNER JOIN admins as adm
					ON evts.creator = adm.id
				INNER JOIN v_subjects AS sub
					ON sub.ni = adm.subject
				INNER JOIN allowed_forks_event AS fo_evt
					ON fo_evt.event_id = evts.id
				INNER JOIN forks AS fo
					ON fo.id = fo_evt.fork_id
				GROUP BY evts.id, evts.creator, adm.subject, sub.full_name, evts.name, evts.event_date
				ORDER BY evts.event_date DESC
		""")

		return [row for row in cur]


def get_date_range_events(date_from: Optional[Union[str, date]], date_to: Optional[Union[str, date]]) -> List[dict]:
	where, data = list(), list()

	if date_from is not None:
		where.append('evts.event_date >= %s')
		data.append(date_from)

	if date_to is not None:
		where.append('evts.event_date <= %s')
		data.append(date_to)

	if len(where) != 0:
		where = 'WHERE ' + ' AND '.join(where)
	else:
		where = ''

	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				evts.id,
				evts.creator,
				adm.subject AS creator_ni,
				sub.full_name AS creator_name,
				evts.name,
				evts.event_date,
				ARRAY_AGG(('{' ||
					'"id": '    || fo.id   || ', '  ||
					'"name": "' || fo.name || '", ' ||
					'"fork": "' || fo.fork || '" '  ||
				'}')::JSON) as forks
			FROM events AS evts
				INNER JOIN admins as adm
					ON evts.creator = adm.id
				INNER JOIN v_subjects AS sub
					ON sub.ni = adm.subject
				INNER JOIN allowed_forks_event AS fo_evt
					ON fo_evt.event_id = evts.id
				INNER JOIN forks AS fo
					ON fo.id = fo_evt.fork_id
				%s
				GROUP BY evts.id, evts.creator, adm.subject, sub.full_name, evts.name, evts.event_date
				ORDER BY evts.event_date DESC
		""" % where, tuple(data))

		return [row for row in cur]


def get_events_limit(limit: int = 5) -> List[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				evts.id,
				evts.creator,
				adm.subject AS creator_ni,
				sub.full_name AS creator_name,
				evts.name,
				evts.event_date,
				ARRAY_AGG(('{' ||
					'"id": '    || fo.id   || ', '  ||
					'"name": "' || fo.name || '", ' ||
					'"fork": "' || fo.fork || '" '  ||
				'}')::JSON) as forks
			FROM events AS evts
				INNER JOIN admins as adm
					ON evts.creator = adm.id
				INNER JOIN v_subjects AS sub
					ON sub.ni = adm.subject
				INNER JOIN allowed_forks_event AS fo_evt
					ON fo_evt.event_id = evts.id
				INNER JOIN forks AS fo
					ON fo.id = fo_evt.fork_id
				GROUP BY evts.id, evts.creator, adm.subject, sub.full_name, evts.name, evts.event_date
				ORDER BY evts.event_date DESC
				LIMIT %s
		""", (limit, ))

		return [row for row in cur]


def get_registered_subjects(evt_id: int) -> List[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				reg_sub.id,
				sub.ni,
				ARRAY_AGG(sub.ni_type) AS ni_type,
				ARRAY_AGG(sub.fork) AS fork,
				sub.full_name,
				sub.birthday,
				sub.email,
				sub.phone,
				ARRAY_AGG(('{' ||
					'"id": ' || poss_afc.id || ', ' ||
					'"description": "' || poss_afc.description || '" ' ||
				'}')::JSON) AS symptoms,
				reg_sub.observations
			FROM registered_subjects AS reg_sub
				INNER JOIN v_subjects AS sub
					ON reg_sub.subject = sub.ni
				LEFT JOIN registered_subjects_symptoms as reg_afc
					ON reg_afc.registered_subject = reg_sub.id
				LEFT JOIN possibles_symptoms AS poss_afc
					ON poss_afc.id = reg_afc.symptom
				WHERE reg_sub.event_id = %s
				GROUP BY reg_sub.id, sub.ni, sub.full_name, sub.birthday,
						sub.email, sub.phone, reg_sub.observations
		""", (evt_id, ))

		res = []

		for row in cur:
			res.append(dict(row))

			last = len(res) - 1
			res[last]['ni_type'] = res[last]['ni_type'].pop()
			res[last]['fork'] = res[last]['fork'].pop()
			res[last]['symptoms'] = list(filter(lambda x: x is not None, res[last]['symptoms']))

		return res


def get_registered_temperature(evt_id: int) -> List[dict]:
	with AutoHandledCursor(get_client()) as cur:
		cur.execute("""
			SELECT
				reg_temp.id,
				sub.ni,
				sub.ni_type,
				sub.fork,
				sub.full_name,
				sub.birthday,
				sub.email,
				sub.phone,
				reg_temp.initial_temp,
				reg_temp.final_temp
				FROM registered_temperature AS reg_temp
				INNER JOIN v_subjects AS sub
					ON reg_temp.subject = sub.ni
				WHERE reg_temp.event_id = %s
		""", (evt_id, ))

		return list(iter(cur))
