#!/bin/bash

REPO_DIR=$(dirname $0)
FILENAME=$0

usage() {
	echo -e "usage: $FILENAME [--service-mode single|multiple|none] [--python-venv 0|1] [--python-path PYTHON_EXEC]"
	echo -e "       $FILENAME -h|--help"
	echo
	echo -e "\t{{{ Service Installation }}}"
	echo -e "\t--service-mode\t-> Install service mode for single or mutiple instances, none for no install service on systemd"
	echo -e "\t{{{ Python Interpreter Configuration }}}"
	echo -e "\t--python-venv\t-> Installation mode with or without virtual environment, by default (0)"
	echo -e "\t--python-path\t-> Set default python interpreter path, by default \"$(which python)\""

	exit $1
}

service_mode=single
use_venv=0
python_path=$(which python)

while [[ $# -gt 0 ]]; do
	key=$1
	value=$2

	case $key in
		-h | --help )
			usage 0
			;;

		--service-mode )
			service_mode=$value
			shift; shift
			;;

		--python-venv )
			use_venv=$value
			shift; shift
			;;

		--python-path )
			python_path=$value
			shift; shift
			;;

		* )
			echo "ERROR: unknown argument \"$key\""
			usage 1
			;;
	esac
done

# Create distribution folder structure
echo "=== Creating dir structure ==="
DIST_DIR=$REPO_DIR/dist
if [ ! -d $DIST_DIR ]; then
	mkdir $DIST_DIR
fi

mkdir -p $DIST_DIR/etc/bio_scout.d
echo "mkdir $DIST_DIR/etc/bio_scout.d"
mkdir -p $DIST_DIR/usr/lib/bio_scout
echo "mkdir $DIST_DIR/usr/lib/bio_scout"
mkdir -p $DIST_DIR/lib/systemd/system
echo "mkdir $DIST_DIR/lib/systemd/system"

# Copy App content
echo "=== Copy content ==="
cp -rv $REPO_DIR/*.py $DIST_DIR/usr/lib/bio_scout/.
cp -rv $REPO_DIR/resources/run.sh $DIST_DIR/usr/lib/bio_scout/.
for dir in controllers database errors locale routes session static utils views; do
	cp -rv $REPO_DIR/$dir $DIST_DIR/usr/lib/bio_scout/.
done
cp -rv $REPO_DIR/config.toml $DIST_DIR/etc/bio_scout.d/config.toml

echo "=== Saving env file ==="
echo "PYTHON_PATH=${python_path}"    > $REPO_DIR/.env
echo "USE_VENV=${use_venv}"         >> $REPO_DIR/.env
echo "SERVICE_MODE=${service_mode}" >> $REPO_DIR/.env