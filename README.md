# Bio-Scout
Bio-Security Form for Scout Activities and Camps

## Objectives
- [X] Participants, Parents and Administrators Control and Auto Complete
- [X] Exportable events data
- [X] Details of Activities' sign up
- [X] Authorization Document for children
- [ ] WPA Support **(Pending for logos)**
- [X] Full Responsive Design support
- [ ] Locale support for other countries
- [X] Customizable installation
- [X] Subjects, Events & Administrators summaries
- [X] Other Configurations (Group Forks, Affections)
- [X] Password Change
- [X] Easy Reading Documentation
- [X] Credits Page ;)

## Application Contributors
- [**SGT911**](https://gitlab.com/SGT911)

### Design Contribution
- **Isabella Morales** (Logo designer)

## Locales Support & Contributors
- **es_CO**: [SGT911](https://gitlab.com/SGT911)

## Technologies
- **Documentation**:
	+ **Framework**: [Vuepress](https://v1.vuepress.vuejs.org/)
- **FrontEnd**:
	+ **Preprocessing**: [Tornado Template](https://www.tornadoweb.org/en/stable/guide/templates.html) engine like [Jinja2](https://jinja.palletsprojects.com/)
	+ **JS Framework**: [Vue.js](https://vuejs.org/)
	+ **CSS Framework**: [Bulma CSS](https://bulma.io/)
- **BackEnd**:
	+ **Web Server**: [Python](https://www.python.org/) with [Tornado](https://github.com/tornadoweb/tornado#tornado-web-server)
	+ **Database**: [PostgreSQL](https://www.postgresql.org/)
	+ **Session Database**: [RedisDB](https://redis.io/)
