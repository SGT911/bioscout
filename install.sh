#!/bin/bash

REPO_DIR=$(dirname $0)
DIST_DIR=$REPO_DIR/dist
FILENAME=$0

if [ $(id -u) -ne 0 ]; then
	echo "DANGER: The script must be run as root"
	exit 1
fi

if [ -f $REPO_DIR/.env ]; then
	source $REPO_DIR/.env
else
	echo "First run configure.sh script"
	exit 1
fi

cp -rv $DIST_DIR/* /.

if [ $SERVICE_MODE != "none" ]; then
	if [ $SERVICE_MODE = "single" ]; then
		cp -rv $REPO_DIR/resources/systemd/single.service /lib/systemd/system/bio_scout.service
	else
		cp -rv $REPO_DIR/resources/systemd/multiple.service /lib/systemd/system/bio_scout\@.service
	fi
fi

# Check if need create virtualenv
if [ $USE_VENV -eq 1 ]; then
	$PYTHON_PATH -m virtualenv --help > /dev/null > /dev/null
	has_virtualenv=$?
	if [ $has_virtualenv -ne 0 ]; then
		$PYTHON_PATH -m pip install virtualenv
	fi

	$PYTHON_PATH -m virtualenv /usr/lib/bio_scout/.venv

	source /usr/lib/bio_scout/.venv/bin/activate
	pip install -r $REPO_DIR/requirements.txt
	python -m compileall /usr/lib/bio_scout
	deactivate
else
	$PYTHON_PATH -m pip install -r $REPO_DIR/requirements.txt
	$PYTHON_PATH -m compileall /usr/lib/bio_scout
fi