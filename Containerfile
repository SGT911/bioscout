FROM docker.io/library/python:3.9-buster
MAINTAINER sgt911 <sgt.911@outlook.com>

ENV APP_LANG es
ENV APP_LOCALE es_CO

ENV PG_DBNAME bioscout
ENV PG_HOST database
ENV PG_PORT 5432
ENV PG_USER postgres
ENV PG_PASSWD 'postgres admin'

ENV REDIS_HOST redis
ENV REDIS_PORT 6379
ENV REDIS_DB 0
# ENV REDIS_PASSWD 'redis_passwd'

ENV TEX_SCHEME basic

RUN apt update; \
	apt upgrade -y; \
	rm -rf /var/lib/apt/lists/*

ADD http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz /install-tl-unx.tar.gz
RUN tar xzf install-tl-unx.tar.gz; \
	rm install-tl-unx.tar.gz; \
	cd install-tl-*; \
	echo 'I' | ./install-tl -scheme $TEX_SCHEME -no-verify-downloads; \
	cd / && rm -rdf install-tl-*;
RUN export TL_VER=$(ls /usr/local/texlive/ | egrep '[0-9]+'); \
	export TL_ARCH=$(ls /usr/local/texlive/${TL_VER}/bin); \
	for f in $(ls "/usr/local/texlive/${TL_VER}/bin/${TL_ARCH}"); do ln -sf /usr/local/texlive/${TL_VER}/bin/${TL_ARCH}/${f} /usr/bin/${f}; done
RUN tlmgr install fancyhdr draftwatermark helvetic

WORKDIR /tmp/bioscout
COPY . .
RUN ./configure.sh --service-mode none && ./install.sh

WORKDIR /
RUN rm -rdf /tmp/bioscout

COPY ./kube/local/config.toml /etc/bio_scout.d/config.toml
RUN cookie_secret=$(openssl rand 30 | openssl dgst -sha512 | awk '{print $2}'); \
	sed -i "s/__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__/$cookie_secret/" /etc/bio_scout.d/config.toml

EXPOSE 80
CMD /usr/lib/bio_scout/run.sh /etc/bio_scout.d/config.toml