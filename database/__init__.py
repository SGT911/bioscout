from psycopg2 import connect
from psycopg2.extras import RealDictCursor

from config import DATA as ENV

from os import listdir
from os.path import dirname, abspath, join
import re

DB = None


def get_client():
	global DB

	if DB is None:
		DB = connect(
			**ENV['database']['postgres'],
			cursor_factory=RealDictCursor,
		)

	return DB


def install_db():
	folder = join(dirname(abspath(__file__)), 'scripts')
	scripts = list(filter(lambda s: re.match(r'^.+\.sql', s) is not None, listdir(folder)))
	scripts.sort()

	scripts = list(map(lambda s: f'{folder}/{s}', scripts))

	with AutoHandledCursor(get_client()) as cur:
		for f in scripts:
			with open(f, 'r') as file:
				data = file.read()

				try:
					cur.execute(data)
				except Exception as e:
					print(f'Error on file: "{f}"')
					get_client().rollback()
					raise e
				else:
					get_client().commit()

		try:
			with open(join(folder, '..', '..', 'locale', 'data', f"{ENV['config']['locale']}.sql"), 'r') as file:
				cur.execute(file.read())
		except Exception as e:
			print(f"Error on file: \"{ENV['config']['locale']}\"")
			get_client().rollback()
			raise e
		else:
			get_client().commit()


class AutoHandledCursor:
	def __init__(self, conn):
		self.conn = conn

	def __enter__(self):
		self.cur = self.conn.cursor()
		return self.cur

	def __exit__(self, *args):
		self.cur.close()