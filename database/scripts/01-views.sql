-----------
-- Views --
-----------

CREATE VIEW v_subjects
	AS SELECT
		sub.ni,
		('{' ||
			'"id": '           || nt.id          || ', '  ||
			'"name": "'        || nt.name        || '", ' ||
			'"abbreviation": "' || nt.abbreviation || '" '  || 
		'}')::JSON AS ni_type,
		('{' ||
			'"id": '    || fo.id   || ', '  ||
			'"name": "' || fo.name || '", ' ||
			'"fork": "' || fo.fork || '" '  ||
		'}')::JSON AS fork,
		pe_sub.full_name,
		sub.birthday,
		pe_sub.email,
		pe_sub.phone,
		sub.parent
	FROM subjects AS sub
		INNER JOIN people as pe_sub
			ON pe_sub.ni = sub.ni
		INNER JOIN ni_types AS nt
			ON nt.id = pe_sub.ni_type
		INNER JOIN forks AS fo
			ON fo.id = sub.fork;
