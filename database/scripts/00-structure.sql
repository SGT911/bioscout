---------------
-- Structure --
---------------

-- Key -> Value
CREATE TABLE key_value (
	key VARCHAR(512) NOT NULL,
	value JSON NULL,

	PRIMARY KEY (key)
);

-- Forks
CREATE SEQUENCE id_forks_seq START 1 INCREMENT BY 1;
CREATE TABLE forks (
	id INT NOT NULL DEFAULT nextval('id_forks_seq'::REGCLASS),
	fork VARCHAR(40) NOT NULL,
	name VARCHAR(100) NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (fork)
);

-- Subjects
CREATE SEQUENCE id_ni_types_seq START 1 INCREMENT BY 1;
CREATE TABLE ni_types (
	id INT NOT NULL DEFAULT nextval('id_ni_types_seq'::REGCLASS),
	name VARCHAR(120) NOT NULL,
	abbreviation VARCHAR(6) NOT NULL,

	PRIMARY KEY (id),
	UNIQUE (abbreviation)
);

CREATE TABLE people (
	ni BIGINT NOT NULL,
	ni_type INT NOT NULL,

	full_name VARCHAR(250) NOT NULL,
	email VARCHAR(120) NOT NULL,
	phone BIGINT NOT NULL,

	PRIMARY KEY (ni),
	FOREIGN KEY (ni_type) REFERENCES ni_types (id)
);


CREATE TABLE subjects (
	ni BIGINT NOT NULL,

	fork INT NOT NULL,
	birthday DATE NOT NULL,

	parent BIGINT NULL,

	PRIMARY KEY (ni),
	FOREIGN KEY (ni) REFERENCES people (ni),
	FOREIGN KEY (parent) REFERENCES people (ni),
	FOREIGN KEY (fork) REFERENCES forks (id)
);

-- Admins
CREATE SEQUENCE id_admins_seq START 1 INCREMENT BY 1;
CREATE TABLE admins (
	id INT NOT NULL DEFAULT nextval('id_admins_seq'::REGCLASS),
	subject BIGINT NOT NULL,
	password CHAR(128) NOT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY (subject) REFERENCES subjects (ni),
	UNIQUE (subject)
);

-- Events
CREATE SEQUENCE id_events_seq START 1 INCREMENT BY 1;
CREATE TABLE events (
	id INT NOT NULL DEFAULT nextval('id_events_seq'::REGCLASS),
	creator INT NOT NULL,
	name VARCHAR(120) NOT NULL,
	event_date DATE NOT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY (creator) REFERENCES admins (id)
);

CREATE TABLE allowed_forks_event (
	event_id INT NOT NULL,
	fork_id INT NOT NULL,

	PRIMARY KEY (event_id, fork_id),
	FOREIGN KEY (event_id) REFERENCES events (id),
	FOREIGN KEY (fork_id) REFERENCES forks (id)
);

---- Registered Subjects
CREATE SEQUENCE id_possibles_symptoms_seq START 1 INCREMENT BY 1;
CREATE TABLE possibles_symptoms (
	id INT NOT NULL DEFAULT nextval('id_possibles_symptoms_seq'::REGCLASS),
	description VARCHAR(60) NOT NULL,

	PRIMARY KEY (id)
);

CREATE SEQUENCE id_registered_subjects_seq START 1 INCREMENT BY 1;
CREATE TABLE registered_subjects (
	id INT NOT NULL DEFAULT nextval('id_registered_subjects_seq'::REGCLASS),
	event_id INT NOT NULL,
	subject BIGINT NOT NULL,
	observations TEXT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY (event_id) REFERENCES events (id),
	FOREIGN KEY (subject) REFERENCES subjects (ni),
	UNIQUE (event_id, subject)
);

CREATE TABLE registered_subjects_symptoms (
	registered_subject INT NOT NULL,
	symptom INT NOT NULL,

	PRIMARY KEY (registered_subject, symptom),
	FOREIGN KEY (registered_subject) REFERENCES registered_subjects (id),
	FOREIGN KEY (symptom) REFERENCES possibles_symptoms (id)
);

---- Registered Temperature
CREATE SEQUENCE id_registered_temperature_seq START 1 INCREMENT BY 1;
CREATE TABLE registered_temperature (
	id INT NOT NULL DEFAULT nextval('id_registered_temperature_seq'::REGCLASS),
	event_id INT NOT NULL,
	subject BIGINT NOT NULL,
	initial_temp NUMERIC NOT NULL,
	final_temp NUMERIC NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (event_id) REFERENCES events (id),
	FOREIGN KEY (subject) REFERENCES subjects (ni),
	UNIQUE (event_id, subject)
);