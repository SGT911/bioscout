from abc import ABC
from tornado.web import RequestHandler
from tornado.template import Loader

from session import session as get_session, Session

from controllers.subjects import get_from_ni, get_ni_types, add_ni_type


class NiTypesHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		ni_types = get_ni_types()
		user = get_from_ni(int(session['user_ni']))
		
		template = self.loader.load('admins/ni_types.jinja').generate(
			user=user,
			ni_types=ni_types,
		)
		return self.finish(template)

	def post(self):
		abbreviation = self.get_body_argument('abbreviation').upper()
		name = self.get_body_argument('name').lower()

		add_ni_type(abbreviation, name)

		return self.get()