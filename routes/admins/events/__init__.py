from abc import ABC
from tornado.web import RequestHandler
from tornado.template import Loader

from session import session as get_session, Session
from utils.controllers import NotFoundHandler

from controllers.subjects import get_from_ni, get_forks
from controllers.events import get_one_event, get_date_range_events, get_registered_subjects, get_registered_temperature

from functools import reduce
from typing import List


class CreateEventHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		user = get_from_ni(int(session['user_ni']))
		forks = get_forks()

		template = self.loader.load('admins/events/create.jinja').generate(user=user, forks=forks)
		return self.finish(template)


class GetAllEventsHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		def reduce_handler(acc: List[dict], el: dict) -> List[dict]:
			data = dict(**el)

			data['registered_subjects'] = len(get_registered_subjects(data['id']))
			data['registered_temperatures'] = len(get_registered_temperature(data['id']))

			acc.append(data)
			return acc

		date_from, date_to = self.get_argument('date_from', None, True), self.get_argument('date_to', None, True)
		events = reduce(reduce_handler, get_date_range_events(date_from, date_to), list())

		filter_fork = self.get_argument('fork', None, True)
		if filter_fork is not None:
			filter_fork = int(filter_fork)

			def filter_validation(event: dict) -> bool:
				forks = [ fork['id'] for fork in event['forks'] ]

				return filter_fork in forks

			events = list(filter(filter_validation, events))

		forks = get_forks()
		user = get_from_ni(int(session['user_ni']))

		template = self.loader.load('admins/events/get_all.jinja').generate(
			user=user,
			events=events,
			forks=forks,
		)
		return self.finish(template)


class GetOneEventHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session, *args):
		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		evt_id = int(args[0])

		event=get_one_event(evt_id)
		if event is not None:
			inscribed = get_registered_subjects(evt_id)
			temp_registered = get_registered_temperature(evt_id)
			user = get_from_ni(int(session['user_ni']))
			host = f'{self.request.protocol}://{self.request.host}'

			template = self.loader.load('admins/events/get_one.jinja').generate(
				event=event,
				host=host,
				user=user,
				inscribed=inscribed,
				temp_registered=temp_registered,
			)
			return self.finish(template)

		return NotFoundHandler.prepare(self)
