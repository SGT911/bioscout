from abc import ABC
from tornado.web import RequestHandler
from tornado.template import Loader

from utils.controllers import NotFoundHandler
from controllers.events import get_one_event, get_registered_subjects, get_registered_temperature
from controllers.subjects import get_from_ni, get_forks
from session import session as get_session, Session

from typing import List
from functools import reduce


class TemperatureEventHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session, evt_id: int):
		evt_id = int(evt_id)

		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		event = get_one_event(evt_id)
		if event is None:
			return NotFoundHandler.prepare(self)

		subjects, subjects_temps = get_registered_subjects(evt_id), get_registered_temperature(evt_id)
		subjects_temps = {subject['ni']: subject for subject in subjects_temps}
		
		fork = self.get_argument('fork', None, True)
		if fork is not None:
			fork = int(fork)

		def add_subject(acc: List[dict], el: dict) -> List[dict]:
			if fork is None or fork == el['fork']['id']:
				data = {
					'ni': el['ni'],
					'ni_type': el['ni_type']['abbreviation'],
					'fork': el['fork']['id'],
					'initial_temp': None,
					'final_temp': None,
				}

				if el['ni'] in subjects_temps:
					data['initial_temp'] = subjects_temps[el['ni']]['initial_temp']
					data['final_temp'] = subjects_temps[el['ni']]['final_temp']

				acc.append(data)
				
			return acc

		subjects = reduce(add_subject, subjects, list())
		user = get_from_ni(int(session['user_ni']))
		forks = get_forks()

		template = self.loader.load('admins/events/temperature.jinja').generate(
			event=event,
			subjects=subjects,
			user=user,
			forks=forks,
			fork_filter=fork,
		)
		return self.finish(template)