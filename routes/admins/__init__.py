from abc import ABC
from tornado.web import RequestHandler
from tornado.template import Loader

from session import session as get_session, Session
from utils.controllers import NotFoundHandler

from controllers.subjects import get_from_ni, get_forks
from controllers.admins import get_possible_admins, create_admin


class AdminChangePasswordHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		user = get_from_ni(int(session['user_ni']))

		template = self.loader.load('admins/change_password.jinja').generate(user=user)
		return self.finish(template)


class AdminCreateHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		possible_admins = get_possible_admins()

		filter_fork = self.get_argument('fork', None, True)
		if filter_fork is not None:
			filter_fork = int(filter_fork)

			def filter_handler(el: dict) -> bool:
				return filter_fork == el['fork']['id']

			possible_admins = list(filter(filter_handler, possible_admins))

		user = get_from_ni(int(session['user_ni']))
		forks = get_forks()

		template = self.loader.load('admins/create.jinja').generate(
			user=user,
			forks=forks,
			possible_admins=possible_admins,
		)
		return self.finish(template)

	def post(self):
		ni = int(self.get_body_argument('admin'))

		create_admin(ni, '1234')

		return self.redirect('/')