from abc import ABC
from tornado.web import RequestHandler
from tornado.template import Loader

from session import session as get_session, Session

from controllers.symptoms import get_possible_symptoms, add_symptom
from controllers.subjects import get_from_ni


class SymptomsHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		symptoms = get_possible_symptoms()
		user = get_from_ni(int(session['user_ni']))
		
		template = self.loader.load('admins/symptoms.jinja').generate(
			user=user,
			symptoms=symptoms,
		)
		return self.finish(template)

	def post(self):
		description = self.get_body_argument('description').lower()

		add_symptom(description)

		return self.get()
		