from abc import ABC
from tornado.web import RequestHandler
from tornado.template import Loader

from session import session as get_session, Session
from utils.controllers import NotFoundHandler

from controllers.subjects import get_all_subjects, get_from_ni, get_person_from_ni, get_forks

from functools import reduce
from typing import List


class GetAllSubjectsHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		subjects = get_all_subjects()

		filter_fork = self.get_argument('fork', None, True)
		if filter_fork is not None:
			filter_fork = int(filter_fork)

			def filter_handler(el: dict) -> bool:
				return el['fork']['id'] == filter_fork

			subjects = list(filter(filter_handler, subjects))
			
		forks = get_forks()
		user = get_from_ni(int(session['user_ni']))

		template = self.loader.load('admins/subjects/get_all.jinja').generate(
			user=user,
			subjects=subjects,
			forks=forks,
		)
		return self.finish(template)


class GetOneSubjectsHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session, ni: int):
		ni = int(ni)

		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		subject = get_from_ni(ni)
		if subject is None:
			NotFoundHandler.prepare(self)

		if subject['parent'] is not None:
			subject['parent'] = get_person_from_ni(subject['parent'])

		user = get_from_ni(int(session['user_ni']))

		template = self.loader.load('admins/subjects/get_one.jinja').generate(
			user=user,
			subject=subject,
		)
		return self.finish(template)