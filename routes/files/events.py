from abc import ABC
from tornado.web import RequestHandler
from utils.files import get_filename, create_excel, AutoHandledFile

from controllers.events import get_one_event, get_registered_subjects, get_registered_temperature
from controllers.subjects import get_person_from_ni, get_from_ni
from controllers.symptoms import get_possible_symptoms

from utils.controllers import NotFoundHandler


class EventSummaryHandler(RequestHandler, ABC):
	def get(self, evt_id: int):
		evt_id = int(evt_id)
		event = get_one_event(evt_id)
		if event is None:
			return NotFoundHandler.prepare(self)

		with AutoHandledFile(get_filename('xlsx'), create_excel) as (file, file_content):
			# Sheets
			sheet_subjects = file.add_worksheet('Participantes')
			sheet_temps = file.add_worksheet('Temperaturas')
			sheet_symptoms = file.add_worksheet('Posibles Sintomas')

			# Styles
			title_style = file.add_format({
				'bold': True,
				'italic': True,
				'font_size': 20,
				'font_color': '#538dd5',
				'align': 'center',
				'valign': 'vcenter',
			})
			table_title_style = file.add_format({
				'bold': True,
			})
			bad_subject_style = file.add_format({
				'bold': True,
				'font_color': 'red',
				'num_format': '#0',
			})
			normal_style = file.add_format({
				'num_format': '#0',
			})
			bad_temp_style = file.add_format({
				'bold': True,
				'font_color': 'red',
				'num_format': '#0.00°C',
			})
			normal_temp_style = file.add_format({
				'num_format': '#0.00°C',
			})
			warning_style = file.add_format({
				'bold': True,
				'italic': True,
				'font_color': 'gray',
				'align': 'center',
			})

			# Writing subjects
			sheet_subjects.merge_range(
				0, 0,
				3, 9,
				f"Participantes del evento \"{event['name'].title()}\"", title_style
			)

			col = 0
			titles = ['N° Identificacion', 'Nombre', 'Rama', 'Correo', 'Telefono', 'Sintomas', 'Otras Obsevaciones', 'Nombre Del Acudiente', 'Correo Del Acudiente', 'Telefono Del Acudiente']
			col_len = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			for title in titles:
				sheet_subjects.write(5, col, title, table_title_style)
				col_len[col] = len(title) + 2
				col += 1

			sheet_subjects.autofilter(5, 0, 5, 9)

			subjects, row = get_registered_subjects(evt_id), 6
			if len(subjects) > 0:
				for subject in subjects:
					style = normal_style
					if subject['observations'] is not None or len(subject['symptoms']) > 0:
						style = bad_subject_style

					sheet_subjects.write(row, 0, subject['ni'], style)
					sheet_subjects.write(row, 1, subject['full_name'].title(), style)
					sheet_subjects.write(row, 2, subject['fork']['fork'].upper(), style)
					sheet_subjects.write(row, 3, subject['email'], style)
					sheet_subjects.write(row, 4, subject['phone'], style)

					if len(subject['symptoms']) == 0:
						subject_symptoms = 'N/A'
					else:
						subject_symptoms = ', '.join(map(str, sorted(map(lambda af: af['id'], subject['symptoms']))))

					sheet_subjects.write(row, 5, subject_symptoms, style)

					if subject['observations'] is None:
						subject_observations = 'N/A'
					else:
						subject_observations = subject['observations']

					sheet_subjects.write(row, 6, subject_observations, style)

					new_widths = [
						(0, str(subject['ni'])),
						(1, subject['full_name']),
						(2, subject['fork']['fork']),
						(3, subject['email']),
						(4, str(subject['phone'])),
						(5, subject_symptoms),
						(6, subject_observations),
					]

					for i, v in new_widths:
						len_v = len(v) + 2
						if col_len[i] < len_v:
							col_len[i] = len_v

					subject = get_from_ni(subject['ni'])
					if subject['parent'] is None:
						sheet_subjects.merge_range(
							row, 7,
							row, 9,
							'Sin Acudiente', warning_style
						)
					else:
						parent = get_person_from_ni(subject['parent'])
						sheet_subjects.write(row, 7, parent['full_name'].title(), style)
						sheet_subjects.write(row, 8, parent['email'], style)
						sheet_subjects.write(row, 9, parent['phone'], style)

						new_widths = [
							(7, parent['full_name']),
							(8, parent['email']),
							(9, str(parent['phone'])),
						]

						for i, v in new_widths:
							len_v = len(v) + 2
							if col_len[i] < len_v:
								col_len[i] = len_v

					row += 1
			else:
				sheet_subjects.merge_range(
					row, 0,
					row, 9,
					'Nadie se a registrado aun', warning_style
				)

			for i in range(len(col_len)):
				sheet_subjects.set_column(i, i, col_len[i])

			# Temperatures writing
			col = 0
			titles = ['N° Identificacion', 'Nombre', 'Rama', 'Temperatura De Ingreso', 'Temperatura De Salida']
			col_len = [0, 0, 0, 0, 0]
			for title in titles:
				sheet_temps.write(0, col, title, table_title_style)
				col_len[col] = len(title) + 2
				col += 1

			sheet_temps.autofilter(0, 0, 0, 4)

			temps = get_registered_temperature(evt_id)

			if len(temps) > 0:
				row = 1
				for temp in temps:
					style, ni_style = normal_temp_style, normal_style
					final_temp = temp['final_temp']
					if final_temp is None:
						final_temp = 'N/A'

					if temp['initial_temp'] >= 37 or (final_temp != 'N/A' and final_temp >= 37):
						style, ni_style = bad_temp_style, bad_subject_style

					sheet_temps.write(row, 0, temp['ni'], ni_style)
					sheet_temps.write(row, 1, temp['full_name'].title(), style)
					sheet_temps.write(row, 2, temp['fork']['fork'].upper(), style)
					sheet_temps.write(row, 3, temp['initial_temp'], style)
					sheet_temps.write(row, 4, final_temp, style)

					new_widths = [
						(0, str(temp['ni'])),
						(1, temp['full_name']),
						(2, temp['fork']['fork']),
						(3, str(temp['initial_temp'])),
						(4, str(final_temp)),
					]

					for i, v in new_widths:
						len_v = len(v) + 2
						if col_len[i] < len_v:
							col_len[i] = len_v

					row += 1
			else:
				sheet_temps.merge_range(
					1, 0,
					1, 4,
					'Nadie se a registrado aun', warning_style
				)

			for i in range(len(col_len)):
				sheet_temps.set_column(i, i, col_len[i])

			# Possibles Afections writing
			symptoms = get_possible_symptoms()
			col = 0
			for tit in ['ID', 'Descripcion']:
				sheet_symptoms.write(0, col, tit, table_title_style)
				col += 1

			row, max_col_len = 1, 0
			for symptom in symptoms:
				sheet_symptoms.write(row, 0, symptom['id'], table_title_style)
				sheet_symptoms.write(row, 1, symptom['description'].title())

				if max_col_len < len(symptom['description']):
					max_col_len = len(symptom['description']) + 2

				row += 1

			sheet_symptoms.set_column(1, 1, max_col_len)

			file.close()
			data = file_content()

		self.set_header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		self.finish(data)