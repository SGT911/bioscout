from abc import ABC
from tornado.web import RequestHandler
from utils.files import get_filename, create_excel, AutoHandledFile

from controllers.subjects import get_person_from_ni, get_all_subjects, get_forks, get_ni_types


class SubjectsSummaryHandler(RequestHandler, ABC):
	def get(self):
		with AutoHandledFile(get_filename('xlsx'), create_excel) as (file, file_content):
			# Sheets
			sheet_subjects = file.add_worksheet('Participantes')
			sheet_forks = file.add_worksheet('Ramas')
			sheet_ni_types = file.add_worksheet('Tipos de Identificaciones')

			# Styles
			table_title_style = file.add_format({
				'bold': True,
			})
			normal_style = file.add_format({
				'num_format': '#0',
			})
			not_parent_style = file.add_format({
				'italic': True,
				'align': 'center',
			})

			# Subjects
			col, row = 0, 0
			titles = ['Tipo Identificacion', 'N° Identificacion', 'Nombre', 'Rama', 'Correo', 'Telefono', 'Nombre Del Acudiente', 'Correo Del Acudiente', 'Telefono Del Acudiente']
			col_len = []
			for title in titles:
				sheet_subjects.write(row, col, title, table_title_style)
				col_len.append(len(title) + 2)
				col += 1

			sheet_subjects.autofilter(row, 0, row, col)

			subjects, row = get_all_subjects(), 1
			for subject in subjects:
				sheet_subjects.write(row, 0, subject['ni_type']['abbreviation'], normal_style)
				sheet_subjects.write(row, 1, subject['ni'], normal_style)
				sheet_subjects.write(row, 2, subject['full_name'].title(), normal_style)
				sheet_subjects.write(row, 3, subject['fork']['fork'].upper(), normal_style)
				sheet_subjects.write(row, 4, subject['email'], normal_style)
				sheet_subjects.write(row, 5, subject['phone'], normal_style)

				new_widths = [
					(0, subject['ni_type']['abbreviation']),
					(1, str(subject['ni'])),
					(2, subject['full_name']),
					(3, subject['fork']['fork']),
					(4, subject['email']),
					(5, str(subject['phone'])),
				]

				for i, v in new_widths:
					len_v = len(v) + 2
					if col_len[i] < len_v:
						col_len[i] = len_v

				if subject['parent'] is not None:
					parent = get_person_from_ni(subject['parent'])

					sheet_subjects.write(row, 6, parent['full_name'].title(), normal_style)
					sheet_subjects.write(row, 7, parent['email'], normal_style)
					sheet_subjects.write(row, 8, parent['phone'], normal_style)

					new_widths = [
						(6, parent['full_name']),
						(7, parent['email']),
						(8, str(parent['phone']))
					]

					for i, v in new_widths:
						len_v = len(v) + 2
						if col_len[i] < len_v:
							col_len[i] = len_v
				else:
					sheet_subjects.merge_range(
						row, 6,
						row, 8,
						'Sin Acudiente', not_parent_style
					)

				row += 1

			for i in range(len(col_len)):
				sheet_subjects.set_column(i, i, col_len[i])

			# Forks
			col_len = []
			col = 0
			for tit in ['Rama', 'Nombre']:
				sheet_forks.write(0, col, tit, table_title_style)
				col_len.append(len(tit))
				col += 1

			forks = get_forks()
			row = 1
			for fork in forks:
				sheet_forks.write(row, 0, fork['fork'].upper(), table_title_style)
				sheet_forks.write(row, 1, fork['name'].title())

				new_widths = [
					(0, fork['fork']),
					(1, fork['name']),
				]

				for i, v in new_widths:
					len_v = len(v) + 2
					if col_len[i] < len_v:
						col_len[i] = len_v

				row += 1

			for i in range(len(col_len)):
				sheet_forks.set_column(i, i, col_len[i])

			# Nit Types
			col_len = []
			col = 0
			for tit in ['Abreviacion', 'Nombre']:
				sheet_ni_types.write(0, col, tit, table_title_style)
				col_len.append(len(tit))
				col += 1

			ni_types = get_ni_types()
			row = 1
			for ni_type in ni_types:
				sheet_ni_types.write(row, 0, ni_type['abbreviation'].upper(), table_title_style)
				sheet_ni_types.write(row, 1, ni_type['name'].title())

				new_widths = [
					(0, ni_type['abbreviation']),
					(1, ni_type['name']),
				]

				for i, v in new_widths:
					len_v = len(v) + 2
					if col_len[i] < len_v:
						col_len[i] = len_v

				row += 1

			for i in range(len(col_len)):
				sheet_ni_types.set_column(i, i, col_len[i])

			file.close()
			data = file_content()

		self.set_header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		self.finish(data)