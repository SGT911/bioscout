from abc import ABC
from tornado.web import RequestHandler

from os import system

from controllers.subjects import get_person_from_ni, get_from_ni
from controllers.key_value import get_value

from utils.files import FileIO, get_tex_template, get_filename, AutoHandledFile
from utils.controllers import NotFoundHandler, ForbiddenHandler
from utils.templates import format_date

from config import DATA as ENV

from datetime import date


class ParentAuthPDFHandler(RequestHandler, ABC):
	def get(self, ni: int):
		ni = int(ni)

		subject = get_from_ni(ni)
		if subject is None:
			return NotFoundHandler.prepare(self)

		years = (date.today() - subject['birthday']).days / 365
		if years >= 18:
			return ForbiddenHandler.prepare(self, reason='no se puede acceder a este documento')

		parent = get_person_from_ni(subject['parent'])

		formats = {
			'group_number': str(get_value('group_number')),
			'group_name': get_value('group_name').title(),
			'subject_ni_type': subject['ni_type']['abbreviation'].upper(),
			'subject_ni': str(subject['ni']),
			'subject_name': subject['full_name'].title(),
			'fork': subject['fork']['fork'].title(),
			'parent_name': parent['full_name'].title(),
			'parent_ni_type': parent['ni_type']['abbreviation'].upper(),
			'parent_ni': str(parent['ni']),
			'date': format_date(date.today()),
		}

		template = get_tex_template()
		for k, v in formats.items():
			template = template.replace(f'(({k}))', v)

		compile_file = get_filename('tex')
		pdf_file = compile_file.replace('.tex', '.pdf')
		file = FileIO(compile_file, mode='w')
		file.write(template.encode())
		file.close()

		ex = system(f"cd \"{ENV['config']['files']['temp_folder']}\" && pdflatex -synctex=1 -interaction=nonstopmode \"{compile_file}\"")
		print(ex)

		self.set_header('Content-Type', 'application/pdf')
		self.write(AutoHandledFile(pdf_file, lambda x: None).get_file_content())