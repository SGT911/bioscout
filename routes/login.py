from abc import ABC

from tornado.web import RequestHandler
from tornado.template import Loader
from session import session as get_session, Session, COOKIE_KEY

from controllers.admins import login


class LoginHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if session.exists() and session.key_exist('user_ni'):
			return self.redirect('/')

		template = self.loader.load('login.jinja').generate(error=False)
		return self.finish(template)

	@get_session
	def post(self, session: Session):
		ni = int(self.get_body_argument('ni'))
		password = self.get_body_argument('password')

		if login(ni, password):
			session['user_ni'] = ni
			return self.redirect('/')

		self.set_status(403)
		template = self.loader.load('login.jinja').generate(error=True)
		return self.finish(template)


class LogoutHandler(RequestHandler, ABC):
	@get_session
	def get(self, session: Session):
		session.destroy()

		self.clear_cookie(COOKIE_KEY)

		return self.redirect('/')