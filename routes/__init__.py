import asyncio
from abc import ABC

from typing import List

from tornado.web import RequestHandler
from tornado.template import Loader
from session import session as get_session, Session

from controllers import db_is_installed, is_installed
from controllers.subjects import get_from_ni
from controllers.events import get_events_limit, get_registered_subjects
from database import install_db


class IndexHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	@get_session
	def get(self, session: Session):
		if not db_is_installed():
			template = self.loader.load('db_install.jinja').generate()

			async def wrapper_install():
				install_db()

			asyncio.create_task(wrapper_install())

			self.set_status(500)
			return self.finish(template)

		if not is_installed():
			return self.redirect('/install')

		if not session.exists() or not session.key_exist('user_ni'):
			return self.redirect('/login')

		def filter_subject(fork_id: int, subjects: List) -> List:
			return list(filter(lambda el: el['fork']['id'] == fork_id, subjects))

		user = get_from_ni(int(session['user_ni']))

		events = get_events_limit(3)
		event = None
		subjects_registred = []

		if len(events) >= 1:
			event = events[0]
			subjects_registred = get_registered_subjects(event['id'])

		template = self.loader.load('index.jinja').generate(
			events=events,
			last_event=event,
			subjects_registred=subjects_registred,
			filter_subject=filter_subject,
			user=user,
		)
		return self.finish(template)
