from abc import ABC

from tornado.web import RequestHandler
from tornado.template import Loader

from datetime import date
from utils.controllers import NotFoundHandler, ForbiddenHandler

from controllers.subjects import get_ni_types, get_forks, get_from_ni
from controllers.events import get_one_event, get_registered_subjects
from controllers.symptoms import get_possible_symptoms
from controllers.key_value import get_value


class SignUpHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	def get(self, evt_id: int, fork_id: int):
		evt_id = int(evt_id)
		fork_id = int(fork_id)

		evt = get_one_event(evt_id)
		forks_ids = [ fork['id'] for fork in evt['forks'] ]

		if evt is None:
			return NotFoundHandler.prepare(self)

		diff = evt['event_date'] - date.today()
		if 2 >= diff.days >= 0 and fork_id in forks_ids:
			ni = self.get_argument('ni', 'null', True)

			ni_types = get_ni_types()
			forks = get_forks()

			template = self.loader.load('signup.jinja')
			return self.finish(template.generate(
				ni_types=ni_types,
				forks=forks,
				evt=evt,
				fork_id=fork_id,
				ni=ni,
				group=f"{ get_value('group_number') } { get_value('group_name').title() }",
			))

		return ForbiddenHandler.prepare(self, reason='el formulario no esta disponible por el momento.')


class SignUpSymptomsHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	def get(self, evt_id: int, ni: int):
		evt_id = int(evt_id)
		ni = int(ni)

		evt = get_one_event(evt_id)
		if evt is None:
			return NotFoundHandler.prepare(self)

		subject = get_from_ni(ni)
		if subject is None:
			return NotFoundHandler.prepare(self)

		symptoms = get_possible_symptoms()

		registered_subjects = [row['ni'] for row in get_registered_subjects(evt_id)]

		is_already = ni in registered_subjects

		template = self.loader.load('signup_symptoms.jinja')
		return self.finish(template.generate(
			subject=subject,
			evt=evt,
			symptoms=symptoms,
			is_already=is_already,
			group=f"{ get_value('group_number') } { get_value('group_name').title() }",
		))