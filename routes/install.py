from abc import ABC

from tornado.web import RequestHandler
from tornado.template import Loader

from controllers.subjects import get_ni_types, get_forks


class InstallSecondStepHandler(RequestHandler, ABC):
	def initialize(self, loader: Loader):
		self.loader = loader

	def get(self):
		template = self.loader.load('install/second.jinja')

		ni_types, forks = get_ni_types(), get_forks()

		return self.finish(template.generate(ni_types=ni_types, forks=forks))