from abc import ABC
from utils import APIRequestHandler, APIResponse, APIError

from controllers.subjects import get_from_ni, get_person_from_ni, create_subject, subject_schema
from controllers.subjects import update_subject_schema, update_person, update_subject
from utils.json_schema import validate
from datetime import date

from errors import NotFound, MetaError


class APISubjectParentHandler(APIRequestHandler, ABC):
	def get(self, ni: int):
		ni = int(ni)

		parent = get_person_from_ni(ni)
		if parent is None:
			return self.write_response(404, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(NotFound(f'The parent {ni} was not found')),
				],
			))

		return self.write_response(200, APIResponse(
			status='ok',
			payload=parent,
		))

class APISubjectHandler(APIRequestHandler, ABC):
	def get(self, ni: int):
		ni = int(ni)

		subject = get_from_ni(ni)
		if subject is None:
			subject = get_person_from_ni(ni)
			if subject is None:
				return self.write_response(404, APIResponse(
					status='error',
					errors=[
						APIError.parse_exception(NotFound(f'The subject {ni} was not found')),
					],
				))
			else:
				subject['is_subject'] = False
				subject['parent'] = None
				subject['fork'] = None
				subject['birthday'] = date.today()
		else:
			subject['is_subject'] = True


		if subject['parent'] is not None:
			subject['parent'] = get_person_from_ni(subject['parent'])

		subject['birthday'] = subject['birthday'].isoformat()

		return self.write_response(200, APIResponse(
			status='ok',
			payload=subject,
		))

	def post(self, ni: int):
		ni = int(ni)

		subject = get_from_ni(ni)
		if subject is not None:
			return self.write_response(400, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(MetaError(f'The subject {ni} already exists')),
				],
			))

		data = self.parse_data()
		errors = validate(data, subject_schema)

		if len(errors) > 0:
			return self.write_response(400, APIResponse(
				status='error',
				errors=list(map(lambda ex: APIError.parse_exception(ex), errors)),
			))

		if data['ni'] != ni:
			return self.write_response(400, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(MetaError(f'The data is incongruent')),
				],
			))

		try:
			create_subject(data)
		except Exception as e:
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(e),
				],
			))

		return self.write_response(201, APIResponse(
			status='ok',
			payload='created',
		))

	def put(self, ni: int):
		ni = int(ni)

		subject = get_from_ni(ni)
		if subject is None:
			return self.write_response(404, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(MetaError(f'The subject {ni} does not exists')),
				],
			))

		data = self.parse_data()
		errors = validate(data, update_subject_schema)

		if len(errors) > 0:
			return self.write_response(400, APIResponse(
				status='error',
				errors=list(map(lambda ex: APIError.parse_exception(ex), errors)),
			))

		if data['ni'] != ni:
			return self.write_response(400, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(MetaError(f'The data is incongruent')),
				],
			))

		try:
			subject['ni_type'] = subject['ni_type']['id']
			subject['fork'] = subject['fork']['id']

			for k in iter(['ni_type', 'full_name', 'email', 'phone', 'fork']):
				if k in data and data[k] != subject[k]:
					update_person(ni, k, data[k])

			for k in iter(['birthday', 'fork']):
				if k in data and k == 'birthday':
					year, month, day = tuple(map(lambda x: int(x), data[k].split('-')))
					data[k] = date(year, month, day)

				if k in data and data[k] != subject[k]:
					update_subject(ni, k, data[k])

			if 'parent' in data:
				parent_ni = data['parent']['ni']
				parent_data = data['parent']
				parent = get_person_from_ni(parent_ni)

				for k in iter(['ni_type', 'full_name', 'email', 'phone', 'fork']):
					if k in parent_data and parent_data[k] != parent[k]:
						update_person(parent_ni, k, parent_data[k])
		except Exception as e:
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(e),
				],
			))

		return self.write_response(200, APIResponse(
			status='ok',
			payload='modified',
		))
