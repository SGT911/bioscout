from abc import ABC
from utils import APIRequestHandler, APIResponse, APIError
from utils.json_schema import validate

from controllers import is_installed
from controllers.install import first_step, run_first_step, second_step, run_second_step

from errors import InternalServerError


class APIFirstStepInstallHandler(APIRequestHandler, ABC):
	def post(self):
		if is_installed():
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(RuntimeError('The server is already installed'))
				],
			))

		data = self.parse_data()
		errors = validate(data, first_step)

		if len(errors) > 0:
			return self.write_response(400, APIResponse(
				status='error',
				errors=list(map(lambda ex: APIError.parse_exception(ex), errors)),
			))

		try:
			ok = run_first_step(data)

			if not ok:
				return self.write_response(403, APIResponse(
					status='error',
					errors=[
						APIError.parse_exception(InternalServerError('The first step was not run successfully'))
					],
				))
		except Exception as e:
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(e),
				],
			))

		return self.write_response(200, APIResponse(
			status='ok',
		))


class APISecondStepInstallHandler(APIRequestHandler, ABC):
	def post(self):
		if is_installed():
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(RuntimeError('The server is already installed'))
				],
			))

		data = self.parse_data()
		errors = validate(data, second_step)

		if len(errors) > 0:
			return self.write_response(400, APIResponse(
				status='error',
				errors=list(map(lambda ex: APIError.parse_exception(ex), errors)),
			))

		try:
			ok = run_second_step(data)

			if not ok:
				return self.write_response(403, APIResponse(
					status='error',
					errors=[
						APIError.parse_exception(RuntimeError('This step is already executed')),
					],
				))
		except Exception as e:
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(e),
				],
			))

		return self.write_response(200, APIResponse(
			status='ok',
		))