from abc import ABC
from utils import APIRequestHandler, APIResponse, APIError

from utils.json_schema import validate
from controllers.admins import change_password_schema, change_password

class APIChangePasswordHandler(APIRequestHandler, ABC):
	def post(self):
		data = self.parse_data()
		errors = validate(data, change_password_schema)

		if len(errors) > 0:
			return self.write_response(400, APIResponse(
				status='error',
				errors=list(map(lambda ex: APIError.parse_exception(ex), errors)),
			))

		try:
			change_password(**data)
		except Exception as e:
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(e),
				],
			))

		return self.write_response(200, APIResponse(
			status='ok',
			payload='modified',
		))