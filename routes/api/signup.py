from abc import ABC
from utils import APIRequestHandler, APIResponse, APIError

from controllers.signup import symptoms_schema, signup_symptom
from controllers.signup import temperature_schema, signup_temperature, update_temperature
from controllers.events import get_registered_temperature, get_one_event
from utils.json_schema import validate

from errors import NotFound


class APISignUpSymptoms(APIRequestHandler, ABC):
	def post(self):
		data = self.parse_data()
		errors = validate(data, symptoms_schema)

		if len(errors) > 0:
			return self.write_response(400, APIResponse(
				status='error',
				errors=list(map(lambda ex: APIError.parse_exception(ex), errors)),
			))

		try:
			signup_symptom(data)
		except Exception as e:
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(e),
				],
			))

		return self.write_response(201, APIResponse(
			status='ok',
			payload='created',
		))


class APISignUpTemperatures(APIRequestHandler, ABC):
	def post(self):
		data = self.parse_data()
		errors = validate(data, temperature_schema)

		if len(errors) > 0:
			return self.write_response(400, APIResponse(
				status='error',
				errors=list(map(lambda ex: APIError.parse_exception(ex), errors)),
			))

		if get_one_event(data['event']) is None:
			return self.write_response(404, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(NotFound(f"The event {data['event']} does not exist")),
				],
			))

		try:
			subjects_temps = [subject['ni'] for subject in get_registered_temperature(data['event'])]
			for ni, temps in data['subjects'].items():
				if int(ni) in subjects_temps:
					update_temperature(ni=ni, evt_id=data['event'], **temps)
				else:
					signup_temperature(ni=ni, evt_id=data['event'], **temps)
		except Exception as e:
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(e),
				],
			))

		return self.write_response(200, APIResponse(
			status='ok',
		))
