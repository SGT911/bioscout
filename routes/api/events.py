from abc import ABC
from utils import APIRequestHandler, APIResponse, APIError

from utils.json_schema import validate
from controllers.events import create_event_schema, create_event


class APIEventsHandler(APIRequestHandler, ABC):
	def post(self, *args):
		data = self.parse_data()
		errors = validate(data, create_event_schema)

		if len(errors) > 0:
			return self.write_response(400, APIResponse(
				status='error',
				errors=list(map(lambda ex: APIError.parse_exception(ex), errors)),
			))

		try:
			evt_id = create_event(data)
		except Exception as e:
			return self.write_response(500, APIResponse(
				status='error',
				errors=[
					APIError.parse_exception(e),
				],
			))

		return self.write_response(201, APIResponse(
			status='ok',
			payload=evt_id,
		))

